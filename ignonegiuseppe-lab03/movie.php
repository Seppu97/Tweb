<!-- 
autore : Giuseppe Ignone
La scelta fatta nell'inserire codice php in questa pagina � stata quella di inserire delle porzioni di codice solo nel momento 
in cui serviva davvero ottenere delle informazioni dal database del film. si pu� notare infatti che prima di ogni parte cruciale
viene aperto il tag php per scrivere la porzione di codice che si occupa di estrarre i dati dal "database". Di seguito ci sono altri
commenti utili a capire le linee di codice php.
!NOTE: ci sono anche dei commenti che sono stati a me utili per capire quali informazioni html dovevano essere rese DINAMICHE.
-->
<!DOCTYPE html>
<?php 
    $movie=$_GET["film"]; // recupero banalmente il nome del film cos� da sapere poi quali dati da recuperare
    
    list($title,$year,$rating) = file("$movie/info.txt",FILE_IGNORE_NEW_LINES); // recupero informazioni'info.txt' dalla relativa cartella
    
    if ((int) $rating > 60) {   // verifico quale sar� l'immagine adatta da mettere in header
        $imgheader = "freshbig";
    } else {
        $imgheader = "rottenbig";
    }
?>
<html lang="en">
	<head>
		<title>TMNT - Rancid Tomatoes</title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link href="movie.css" type="text/css" rel="stylesheet">
                <link href="http://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/rotten.gif" type="image/png" rel="shortcut icon">
	</head>

	<body>
            
		<div id="banner">
			<img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/banner.png" alt="Rancid Tomatoes">
		</div>

		<h1 id ="header"><?= $title." ".$year?></h1>   <!--info.txt -->
            <div id="global">
                <div id ="overview">
		<div>
			<img src="<?="$movie/overview.png"?>" alt="general overview"> <!--Overview.png-->
		</div>

		<dl>    <!-- Overview.txt-->
                   <?php
                        
                        foreach(file("$movie/overview.txt") as $dl){
                            list($dt,$dd) = explode(":",$dl);
                    ?>
                        <dt><?=$dt?></dt>
                        <dd><?=$dd?></dd>
                        
                        <?php 
                        }
                        ?>
                       
		</dl>
                </div>
                
                
                <div id ="rotten">
		<div id="rottenheader">
                        <!-- se maggiore di 60% questo sar� un freshbig.png altrimenti rimane rottenbig.png-->
                        <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/<?=$imgheader?>.png" 
                                                                         alt="<?=$imgheader?>">
			<?=$rating."%"?> <!--info.txt -->
		</div>
		
               <div class="column">
                <!-- inizio recensione -->
                
                <?php 
                    $reviews = glob("$movie/review*.txt");  // reviews sar� un array contentente tutte le reviews
                    $numReviews = count($reviews);
                    
                    if($numReviews <= 10 && $numReviews >=1){  // se il numero di reviews � compreso tra 1 e 10 allora tutto a posto
                        $halfreviews = ceil($numReviews / 2); // la prima conterr� la met� delle revies se il numero di revies � pari
                                                              // altrimenti conterr� la met� + 1 come da traccia.
                    }elseif($numReviews > 10){   // se le reviews sono pi� di 10 forzo a mostrarne 5 e 5 .
                        $halfreviews = 5;
                        $numReviews = 10;
                    }elseif($numReviews == 0){  // se sono 0 altrimenti la pagina di recensioni resta vuota.
                        $halfreviews = 0;
                    }
                    
                    for($i = 0 ; $i < $halfreviews;$i++){
                        list($review,$rat,$reviewer,$publication) = file($reviews[$i],FILE_IGNORE_NEW_LINES);
                        //NOTE: il flag FILE_IGNONE_NEW_LINES serve a far ignorare i \n alla funzione file.
                ?>
                
		<p class="firstp">
                        <!-- seconda riga di review.txt (se c'� scritto rotten mettere rotten.gif , se c'� scritto fresh mettere fresh.gif) -->
                        <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/<?=strtolower($rat)?>.gif" alt="<?= strtolower($rat)?>"> 
                        <!-- prima riga di review.txt contiene la recensione -->
			<q><?= $review ?></q>
		</p>
		<p class="secondp">
                        
			<img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/critic.gif" alt="Critic"> <!-- invariata -->
			<?= $reviewer ?> <br><!-- nome recensore terza riga di review.txt -->
			<?= $publication ?> <!-- quarta riga di textview.txt pubblicazione -->
		</p>
               <?php
                    }
               ?>
                  <!-- fine recensione -->  
             
               </div>
               
               
               <div class="column">
                   
                   <?php
                    for(;$i < $numReviews ; $i++){
                        list($review,$rat,$reviewer,$publication) = file($reviews[$i],FILE_IGNORE_NEW_LINES);
                   ?>
		<p class="firstp">
			<img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/<?= strtolower($rat)?>.gif" alt="<?= strtolower($rat)?>">
			<q><?= $review ?></q>
		</p>
		<p class="secondp">
			<img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/critic.gif" alt="Critic">
			<?= $reviewer ?> <br>
			<?= $publication ?>
		</p>
               <?php
                    }
               ?>
                
               </div>
                    
                </div>
                    <p id="pages">(1-<?= $numReviews ?>) of <?=$numReviews?></p>
                </div>
		<div id="validators">
			<a href="ttp://validator.w3.org/check/referer"><img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/w3c-xhtml.png" alt="Validate HTML"></a> <br>
			<a href="http://jigsaw.w3.org/css-validator/check/referer"><img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!"></a>
		</div>
                
                
	</body>
</html>
