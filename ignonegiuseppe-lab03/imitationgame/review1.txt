If a machine is expected to be infallible, it cannot also be intelligent.
FRESH
Alan Turing
Mechanical Intelligence