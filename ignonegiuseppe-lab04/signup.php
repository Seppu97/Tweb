<?php
include("top.html");
?>
<div>
    <form id="fieldset" action="signup-submit.php" method="POST">
        <fieldset class="column">
            <legend>New User Signup:</legend>
            <strong><label class="left" name="labname">Name:</label></strong>
            <input class="match" type="text" name="name" size="16">
            <br>
            
            <strong><label class="left" name="labgender">Gender:</label></strong>
            <input class="match" type="radio" name="gender" value="m">Male
            <input class="match" type="radio" name="gender" value="f" checked="checked">Female
            <br>
            
            <strong><label class="left" name="labage">Age:</label></strong>
            <input class="match" type="text" name="age" size="6" maxlength="2">
            <br>
            
            <strong><label class="left" name="labpersonality">Personality type:</label></strong>
            <input class ="match" type="text" name="personality" size="6" maxlength="4">
            (<a href="http://www.humanmetrics.com/cgi-win/JTypes2.asp">Don't know your type?</a>)
            <br>
            
            <strong><label class="left" name="labfos">Favorite OS</label></strong>
            <select class="match" name="fos">
                <option>Windows</option>
                <option>Mac OS X</option>
                <option selected="selected">Linux</option>
            </select>
            <br>
            
            <strong><label class="left" name="laseekage">Seeking age</label></strong>
            <input class="match" type="text" name="min" placeholder="min" size="6" maxlength="2"> to
            <input class="match" type="text" name="max" placeholder="max" size="6" maxlength="2">
            <br>
            
            <input type="submit" value="Sign Up">
        </fieldset>
    </form>
</div>

<?php
include("bottom.html");
?>