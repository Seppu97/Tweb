-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Creato il: Gen 31, 2019 alle 16:54
-- Versione del server: 5.7.23
-- Versione PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cipenso`
--
CREATE DATABASE IF NOT EXISTS `cipenso` CHARACTER SET utf8 COLLATE utf8_bin;
USE `cipenso`;

-- --------------------------------------------------------

--
-- Struttura della tabella `offers`
--

DROP TABLE IF EXISTS `offers`;
CREATE TABLE IF NOT EXISTS `offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `availability` date DEFAULT NULL,
  `date_offers` date DEFAULT NULL,
  `ended` tinyint(1) DEFAULT '0',
  `accepted` tinyint(1) DEFAULT '0',
  `users` int(11) DEFAULT NULL,
  `request` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users` (`users`,`request`),
  KEY `request` (`request`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `personalcard`
--

DROP TABLE IF EXISTS `personalcard`;
CREATE TABLE IF NOT EXISTS `personalcard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `study` varchar(255) DEFAULT 'non definito',
  `cpi` int(11) DEFAULT '6',
  `rating` int(11) DEFAULT '0',
  `n_rate` int(11) NOT NULL DEFAULT '0',
  `n_contributions` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `request`
--

DROP TABLE IF EXISTS `request`;
CREATE TABLE IF NOT EXISTS `request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(58) NOT NULL,
  `description` text,
  `date_request` date DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `ended` tinyint(1) DEFAULT '0',
  `accepted` tinyint(4) NOT NULL DEFAULT '0',
  `service` int(11) DEFAULT NULL,
  `user` int(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service` (`service`),
  KEY `user` (`user`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `service`
--

DROP TABLE IF EXISTS `service`;
CREATE TABLE IF NOT EXISTS `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(20) DEFAULT NULL,
  `cpi` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE utf8_bin;

--
-- Dump dei dati per la tabella `service`
--

INSERT INTO `service` (`id`, `category_name`, `cpi`) VALUES
(1, 'casa', 4),
(2, 'ripetizioni', 3),
(3, 'elettronica', 6),
(4, 'commissioni', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identitycard` varchar(20) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `firstname` varchar(15) DEFAULT NULL,
  `lastname` varchar(15) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` char(10) DEFAULT NULL,
  `personalcard` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `identitycard` (`identitycard`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `personalcard` (`personalcard`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `vote`
--

DROP TABLE IF EXISTS `vote`;
CREATE TABLE IF NOT EXISTS `vote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voter` int(11) DEFAULT NULL,
  `voted` int(11) DEFAULT NULL,
  `vote` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `voter` (`voter`,`voted`),
  KEY `voted` (`voted`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE utf8_bin;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
