-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Creato il: Feb 05, 2019 alle 09:41
-- Versione del server: 5.7.23
-- Versione PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cipenso`
--
CREATE DATABASE IF NOT EXISTS `cipenso` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `cipenso`;

-- --------------------------------------------------------

--
-- Struttura della tabella `offers`
--

DROP TABLE IF EXISTS `offers`;
CREATE TABLE IF NOT EXISTS `offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `availability` date DEFAULT NULL,
  `date_offers` date DEFAULT NULL,
  `ended` tinyint(1) DEFAULT '0',
  `accepted` tinyint(1) DEFAULT '0',
  `users` int(11) DEFAULT NULL,
  `request` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users` (`users`,`request`),
  KEY `request` (`request`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `offers`
--

INSERT INTO `offers` (`id`, `availability`, `date_offers`, `ended`, `accepted`, `users`, `request`) VALUES
(1, '2019-03-01', '2019-01-31', 0, 1, 2, 1),
(2, '2019-02-17', '2019-01-31', 0, 0, 3, 3),
(3, '2019-02-09', '2019-01-31', 0, 0, 4, 2),
(4, '2019-01-31', '2019-01-31', 0, 0, 4, 4),
(6, '2019-02-15', '2019-02-05', 0, 0, 1, 7),
(7, '2019-02-18', '2019-02-05', 1, 1, 2, 6),
(8, '2019-03-01', '2019-02-05', 0, 0, 2, 7);

-- --------------------------------------------------------

--
-- Struttura della tabella `personalcard`
--

DROP TABLE IF EXISTS `personalcard`;
CREATE TABLE IF NOT EXISTS `personalcard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `study` varchar(255) COLLATE utf8_bin DEFAULT 'non definito',
  `cpi` int(11) DEFAULT '6',
  `rating` int(11) DEFAULT '0',
  `n_rate` int(11) NOT NULL DEFAULT '0',
  `n_contributions` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `personalcard`
--

INSERT INTO `personalcard` (`id`, `study`, `cpi`, `rating`, `n_rate`, `n_contributions`) VALUES
(1, 'non definito', 2, 0, 0, 0),
(2, 'non definito', 0, 0, 0, 0),
(3, 'non definito', 0, 0, 0, 0),
(4, 'non definito', 3, 0, 0, 0),
(5, 'Studente di Informatica (UniTO)', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `request`
--

DROP TABLE IF EXISTS `request`;
CREATE TABLE IF NOT EXISTS `request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(58) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin,
  `date_request` date DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `ended` tinyint(1) DEFAULT '0',
  `accepted` tinyint(4) NOT NULL DEFAULT '0',
  `service` int(11) DEFAULT NULL,
  `user` int(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service` (`service`),
  KEY `user` (`user`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `request`
--

INSERT INTO `request` (`id`, `title`, `description`, `date_request`, `expiry`, `ended`, `accepted`, `service`, `user`) VALUES
(1, 'Montare un armadio grosso', 'Ho da poco acquistato un armadio di grosse dimensioni e avrei bisogno di una persona che mi possa dare una mano a montarlo, in un pomeriggio. ', '2019-01-31', '2019-03-02', 0, 1, 1, 1),
(2, 'Spesa settimanale', 'Sono uno studente in crisi di tempo, avrei bisogno di qualcuno buonanime che faccia la spesa al posto mio ogni giovedì. ', '2019-01-31', '2019-02-18', 0, 0, 4, 2),
(3, 'Ripetizioni di geometria analitica', 'Cerco qualcuno disposto a spiegarmi geometria analitica per un esame. ', '2019-01-31', '2019-03-01', 0, 0, 2, 2),
(4, 'Aggiustare audio del computer', 'Ascoltare la musica su YouTube non è più come prima, ora che l&#39;audio del mio portatile si è manomesso. Qualcuno potrebbe aiutarmi ad aggiustarlo?', '2019-02-05', '2019-01-31', 0, 0, 3, 3),
(5, 'Ritirare un pacco dal deposito', 'Poiché non sono automunito, avrei bisogno di qualcuno che, cortesemente, ritiri al posto mio il pacco e lo porti a casa mia. Il pacco è stato depositato da Bartolini a questo indirizzo : Corso Vigevano 38. Massima urgenza', '2019-01-31', '2019-02-18', 0, 0, 4, 4),
(6, 'Ripetizioni Fisica', 'Cerco Aiuto in Fisica 1 per l&#39;esame del 30/02/2019. In particolare vorrei approfondire argomenti come : campo elettrico,campo magnetico e circuiti.', '2019-02-05', '2019-02-28', 1, 1, 2, 5),
(7, 'Addobbo casa per compleanno', 'Organizzo un compleanno per una mia amica. Avrei bisogno di aiuto per allestire la mia camera per la festa a sorpresa. ', '2019-02-05', '2019-03-02', 0, 0, 4, 5);

-- --------------------------------------------------------

--
-- Struttura della tabella `service`
--

DROP TABLE IF EXISTS `service`;
CREATE TABLE IF NOT EXISTS `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `cpi` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `service`
--

INSERT INTO `service` (`id`, `category_name`, `cpi`) VALUES
(1, 'casa', 4),
(2, 'ripetizioni', 3),
(3, 'elettronica', 6),
(4, 'commissioni', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identitycard` varchar(20) COLLATE utf8_bin NOT NULL,
  `username` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `firstname` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `lastname` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `phone` char(10) COLLATE utf8_bin DEFAULT NULL,
  `personalcard` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `identitycard` (`identitycard`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `personalcard` (`personalcard`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `identitycard`, `username`, `password`, `firstname`, `lastname`, `birthday`, `email`, `phone`, `personalcard`) VALUES
(1, 'ay35467', 'sara-95', '$2y$10$.hcPuLRU3K/1s6B/81ga1eWfeA7znMrOqNhDt6DitF6CKi68GL.Uu', 'Sara', 'Pasquali', '1995-03-03', 'sarapasquali95@gmail.com', '3287543678', 1),
(2, 'ay2839', 'luca.mirto', '$2y$10$J8bMIm0/8.hOXwrJv0Nh8eSrJYSeMLGzpFsm4MWG/c9aeR71iGT8W', 'Luca', 'Mirto', '1998-02-09', 'lucamirto98@yahoo.it', '3287543678', 2),
(3, 'Eybsjab', 'Giorgia98', '$2y$10$7m9GXMA1XNcpZIgi40RyJejCjPA3LxrHPUyOP3pRBdd3FiO/uI/Da', 'Giorgia', 'Spoletti', '1997-05-17', 'giorgiaspoletti@yahoo.it', '3457193719', 3),
(4, 'Ayejsbska', 'lollo-shake', '$2y$10$atoJp5X43c/C4W4dORYrNelBEtqTBMPkfvZnkliXmcdT4qMBicWgu', 'Lorenzo', 'Salentino', '1995-01-13', 'lorenzosalentino@gmail.com', '3719372909', 4),
(5, 'ATJKD', 'prova', '$2y$10$kCEEIS97BvPXlW.quIPaDuC71WLnD/0FI.sHNJS0NG2edBfwSVHxO', 'prova', 'prova', '1997-08-25', 'prova@gmail.com', '3314612423', 5);

-- --------------------------------------------------------

--
-- Struttura della tabella `vote`
--

DROP TABLE IF EXISTS `vote`;
CREATE TABLE IF NOT EXISTS `vote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voter` int(11) DEFAULT NULL,
  `voted` int(11) DEFAULT NULL,
  `vote` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `voter` (`voter`,`voted`),
  KEY `voted` (`voted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
