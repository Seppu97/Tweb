<?php
    session_start();
    require_once 'MVC/MODEL/session/session_function.php';
    check_logged_in();
?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <title>CPI</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
        <link href="MVC/VIEW/profile/menu.css" type="text/css" rel ="stylesheet">
        <link href="MVC/VIEW/request/request.css" type="text/css" rel ="stylesheet">
        <link href="MVC/VIEW/profile/personalcard.css" type="text/css" rel ="stylesheet">
        <link href="MVC/VIEW/profile/star_rating.css" type="text/css" rel ="stylesheet">
        <link href="MVC/VIEW/offers/modal.css" type="text/css" rel ="stylesheet">
        <script src=" https://ajax.googleapis.com/ajax/libs/prototype/1.7.0.0/prototype.js "></script>
        <script src="http://ajax.googleapis.com/ajax/libs/scriptaculous/1.9.0/scriptaculous.js"></script> 
        <script src="js/index.js"></script>
    </head>
    <body>
        <a href="createrequestpage.php"><img id="createrequest" src="img/plus.png" alt="create request"></a><!-- create new requst -->
        <?php
        require_once 'MVC/VIEW/profile/menu.php';
        require_once 'MVC/VIEW/offers/modal.php';
        require_once 'MVC/CONTROLLER/requestcontroller.php';
        
        $controller = new Controller();
        $controller->invoke();
        ?>
    </body>
</html>
 
