<!--this page contains all of my requests. I can acces to all offers for my requests.-->
<?php
    session_start();
    require_once 'MVC/MODEL/session/session_function.php';
    check_logged_in();
?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <title>CPI</title>
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">
        <link href="MVC/VIEW/profile/menu.css" type="text/css" rel ="stylesheet">
        <link href="MVC/VIEW/request/request.css" type="text/css" rel ="stylesheet">
        <script src=" https://ajax.googleapis.com/ajax/libs/prototype/1.7.0.0/prototype.js"></script>
    </head>
    <body>
        <?php
        require_once 'MVC/VIEW/profile/menu.php';
        require_once 'MVC/CONTROLLER/personalrequestcontroller.php';
        
        $mode = filter_input(INPUT_GET, "mode",FILTER_VALIDATE_INT);
        $controller = new Controller();
        if($mode == 1){
            print "<h1>Le tue nuove richieste</h1>";
            $controller->invokeNewRequests($_SESSION['userid']);
        }elseif ($mode == 2){
            print "<h1>Richiese personali acettate</h1>";
            $controller->invokeProgressRequests($_SESSION['userid']);
        }elseif ($mode == 3){
            print "<h1>Richieste portate a termine grazie a cipensoio</h1>";
            $controller->invokeDoneRequests($_SESSION['userid']);
        }else{
            header("Location: errorpage.php?error=2");
        }
        ?>
    </body>
</html>