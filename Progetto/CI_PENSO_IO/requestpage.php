<!-- this page contains all details of a request. Allow users to propose an offer too! -->
<!DOCTYPE html>
<html lang="it">
    <head>
        <title>CPI</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">
        <script  src="js/requestdetails.js"></script>
        <script src=" https://ajax.googleapis.com/ajax/libs/prototype/1.7.0.0/prototype.js "></script>
        <script src="http://ajax.googleapis.com/ajax/libs/scriptaculous/1.9.0/scriptaculous.js"></script> 
        <link href="MVC/VIEW/profile/menu.css" type="text/css" rel ="stylesheet">
        <link href="MVC/VIEW/request/requestdetails.css" type="text/css" rel ="stylesheet">
        
        
    </head>
    <body>
<?php
    session_start();
    require_once 'MVC/MODEL/session/session_function.php';
    check_logged_in();

    require_once 'MVC/CONTROLLER/requestdetailscontroller.php';
    $requestid = filter_input(INPUT_GET, "requestid",FILTER_VALIDATE_INT);
    require_once 'MVC/VIEW/profile/menu.php';
?>

<?php
$controller = new Controller();
$controller->invoke($requestid);

?>

    </body>
</html>