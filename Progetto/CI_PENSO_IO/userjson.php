<?php
require_once 'MVC/MODEL/function_db/query.php';
require_once 'MVC/MODEL/classes/User.php';

$user=NULL;
$username = filter_input(INPUT_POST,'username',FILTER_SANITIZE_STRING);
$email = filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL);
$identitycard = filter_input(INPUT_POST,'identitycard',FILTER_SANITIZE_STRING);
$userid = filter_input(INPUT_POST,'userid',FILTER_VALIDATE_INT);

if($username === NULL && $email === NULL && $identitycard === NULL && $userid === NULL){
    //if there isn't any fields specified.
   
    $i = 0;
    $rows = allUsersQuery();
    
    foreach($rows as $row){

        $user[$i]= new User(
                                    $row['id'],
                                    $row["identitycard"],
                                    $row["username"],
                                    NULL,
                                    $row["firstname"],
                                    $row["lastname"],
                                    $row["birthday"],
                                    $row["email"],
                                    $row["phone"],
                                    $row["personalcard"]
                                );
        $i++;
    }
}elseif( $userid && $username === NULL && $email === NULL && $identitycard === NULL ){
    //get user by id
    
        $rows = userIdQuery($userid);
       
    foreach ($rows as $row){
        $user = new User(
                            $row['id'],
                            $row["identitycard"],
                            $row["username"],
                            NULL,
                            $row["firstname"],
                            $row["lastname"],
                            $row["birthday"],
                            $row["email"],
                            $row["phone"],
                            $row["personalcard"]
                );
    }
}elseif( $username && !($email || $identitycard) ){
    // if only username is specified
    
    $rows = userQuery($username);
        
    foreach($rows as $row){
    
    $user = new User(
                       $row['id'],
                       $row["identitycard"],
                       $row["username"],
                       NULL,
                       $row["firstname"],
                       $row["lastname"],
                       $row["birthday"],
                       $row["email"],
                       $row["phone"],
                       $row["personalcard"]
                    );
}
    
}elseif( $email && $identitycard === NULL){
    //get the user with specified email
    
    $rows = userEmailQuery($email);
    foreach($rows as $row){
    
    $user = new User(
                       $row['id'],
                       $row["identitycard"],
                       $row["username"],
                       NULL,
                       $row["firstname"],
                       $row["lastname"],
                       $row["birthday"],
                       $row["email"],
                       $row["phone"],
                       $row["personalcard"]
                    );
    }
    
}elseif( $email === NULL && $identitycard ){
    //get user with specified  identitycard
    
    $rows = userIdentityQuery($identitycard);
    foreach($rows as $row){
    
    $user = new User(
                       $row['id'],
                       $row["identitycard"],
                       $row["username"],
                       NULL,
                       $row["firstname"],
                       $row["lastname"],
                       $row["birthday"],
                       $row["email"],
                       $row["phone"],
                       $row["personalcard"]
                    );
    }
    
}elseif ( $email && $identitycard && $username === NULL ){
    //get users with (email,identitycard) specified.
    
    $i = 0;
    $rows = userIdentityEmailQuery($email,$identitycard);
    
    foreach($rows as $row){

        $user[$i]= new User(
                                    $row['id'],
                                    $row["identitycard"],
                                    $row["username"],
                                    NULL,
                                    $row["firstname"],
                                    $row["lastname"],
                                    $row["birthday"],
                                    $row["email"],
                                    $row["phone"],
                                    $row["personalcard"]
                                );
        $i++;
    }
    
}elseif($username && $email && $identitycard && $userid !== FALSE){
    //get users with (username,email,identitycard) ALL FIELDS SPECIFIED
   
    $i = 0;
    $rows = userAllDataQuery($username,$email,$identitycard);
    
    foreach($rows as $row){

        $user[$i]= new User(
                                    $row['id'],
                                    $row["identitycard"],
                                    $row["username"],
                                    NULL,
                                    $row["firstname"],
                                    $row["lastname"],
                                    $row["birthday"],
                                    $row["email"],
                                    $row["phone"],
                                    $row["personalcard"]
                                );
        $i++;
    }
}else{
    $user = FALSE;
}



print json_encode($user);  //return all users of my database

?>

