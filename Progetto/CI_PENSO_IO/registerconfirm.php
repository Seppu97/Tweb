<?php
// this page return TRUE is user is correctly registered,NULL if there is some problems with data , FALSE if there is internal error
require_once 'MVC/MODEL/model.php';

 $identitycard = filter_input(INPUT_POST,'identitycard',FILTER_SANITIZE_STRING);
 $username = filter_input(INPUT_POST,'username',FILTER_SANITIZE_STRING);
 $password = filter_input(INPUT_POST,'password',FILTER_SANITIZE_STRING);
 $matche_pw = preg_match('/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/',$password);
 $firstname = filter_input(INPUT_POST,'firstname',FILTER_SANITIZE_STRING);
 $lastname = filter_input(INPUT_POST,'lastname',FILTER_SANITIZE_STRING);
 $birthday_post = filter_input(INPUT_POST,'birthday',FILTER_SANITIZE_STRING);
 $birthday =  DateTime::createFromFormat('Y-m-d',$birthday_post);
 
 if( DateTime::getLastErrors()['warning_count'] > 0 ){
     $birthday = FALSE;
 }
 
 $email = filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL);
 $phone = filter_input(INPUT_POST,'phone',FILTER_SANITIZE_STRING);
 $match_phone = preg_match('/^[0-9]+$/', $phone);

if($identitycard && $username && strlen($username) <= 20 && $matche_pw > 0 && $firstname && $lastname && $birthday 
        && $email && $match_phone > 0 && strlen($phone)=== 10 && $birthday_post <= date("Y-m-d")){
 
    $hash_password = password_hash($password,PASSWORD_DEFAULT);
    $user = new User(
            -1,
            $identitycard,
            $username,
            $hash_password,
            $firstname,
            $lastname,
            $birthday_post,
            $email,
            $phone,
            0
          );


    $model = new Model();
    $response = $model->insertUser($user);

}else{
    $response = NULL;
}

 
print json_encode($response);
 ?>



