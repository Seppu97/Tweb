<?php
/* this page return TRUE if offer date is correctly update, NULL if the date is bad , FALSE if there is internal error*/
session_start();
require_once 'MVC/MODEL/model.php';
$response = NULL;
$model = new Model();
$availability_post = filter_input(INPUT_POST,'availability',FILTER_SANITIZE_STRING);
$availability = DateTime::createFromFormat("Y-m-d",$availability_post);
if( DateTime::getLastErrors()['warning_count'] > 0 || $availability === FALSE){
     $response = FALSE;
}


if($response !== FALSE){
    $userid = $_SESSION['userid'];
    $requestid = $_SESSION['request'];
    $request = $model->getRequest($requestid);
    $offer = $model->getOfferByUserRequest($requestid,$userid);
    
    if($offer){
        if( $availability_post <= $request->expiry && $availability_post >= date("Y-m-d")){
            $offer->availability = $availability_post;
            $response = $model->updateOffer($offer);
         }
     }else{
        $response = FALSE;
    }
}

if($response){
    $response = $availability_post; //response is the new date of offer
}
print json_encode($response);
?>