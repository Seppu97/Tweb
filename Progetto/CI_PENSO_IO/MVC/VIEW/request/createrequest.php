<h1 id="head">Crea la tua richiesta , al resto ci penso io</h1>
<div class="container">
    <form id ="form">
    <div class="row">
      <div class="col-25">
        <label for="title">Titolo</label>
      </div>
      <div class="col-75">
          <input type="text" id="title" name="title"  maxlength="58" placeholder="Titolo richiesta..." required>
      </div>
    </div>
    
    <div class="row">
      <div class="col-25">
        <label for="category">Categoria</label>
      </div>
      <div class="col-75">
        <select id="category" name="category">
            <?php
            foreach($services as $service){
            ?>
          <option class ="category" value="<?= $service->id ?>"><?= $service->category_name ?> (  <?= $service->cpi ?> cpi )</option>
          <?php
            }
          ?>
         
        </select>
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="expiry">Scadenza</label>
      </div>
      <div class ="col-75">
          <input id="expiry" type="date" min="<?= date("Y-m-d") ?>" value="<?=date("Y-m-d") ?>" required>
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="description">Descrizione</label>
      </div>
      <div class="col-75">
        <textarea id="description" name="description" maxlength="400" placeholder="Scrivi qualcosa..." style="height:200px" required></textarea>
      </div>
    </div>
    <div class="row">
      <input id="conferma" type="button" value="Conferma">
    </div>
    </form>
</div>
