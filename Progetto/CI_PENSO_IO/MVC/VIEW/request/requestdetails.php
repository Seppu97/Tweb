<div id="wrapper">
<div id="detailsbox">
    <span id = "header">Dettagli annuncio di <?=$user->firstname." ".$user->lastname?></span>
    <ul>
        <li>categoria : <span>casa</span></li>
        
        <li>scadenza : <span id="expiry"><?=$request->expiry?></span></li>
        <li>Pubblicato il : <span id="date_request"><?=$request->date_request?></span></li>
    </ul>
    
    <div class="circle">
        <?=$service->cpi?>
        <span>cpi</span>
    </div>
</div>
<div id = "mainbox">
    <img id ="categoryimg" src ="img/<?= $service->category_name?>.png" alt=""/> <span id = title><?=$request->title?></span>
    
    <div id = "description"> <?=$request->description?> </div>
    
    <div id="contact">
        <span> Contatti:</span>
        <ul>
            <li>Cellulare : <span><?= $user->phone ?></span></li>
            <li>Email : <span><?=$user->email?></span></li>
        </ul>
     
    </div>
      
</div>
</div>
<div id = "bottom">   
 <?php
        
        $offer = $model->getOfferByUserRequest($request->id,$_SESSION['userid']);
        if($offer !== FALSE){
            if($user->id != $_SESSION['userid']){ //check if is an offer of logged user ( my offer ).
                if($offer){
                    if(intval($offer->accepted) === 0 && $request->expiry >= date("Y-m-d")){
                    ?>
    
                    <div id = "alerttext">Hai dato la disponibilità per questo servizio:
                        <div id="availability_text" class="visible_inline">
                            <span id="date_offer"> Data: <?=$offer->availability?></span>
                            <input class="editbutton" type="image" src="img/edit.png" alt="edit button"> 
                        </div>
                        <div class="invisible_inline"> 
                           <input id="availability_input" type="date" value="<?=$offer->availability?>" min="<?=date("Y-m-d")?>" max="<?=$request->expiry?>"/>
                           <input id="confirm_modify" type="image" src="img/confirm.png" height="32" width="32" alt="confirm modify">
                        </div>
                    </div>
                    
                    
                    <?php
                    }elseif(intval($offer->accepted) === 1 && intval($offer->ended)=== 0){
                      ?>
                        <span id = "alerttext">Congratulazioni , la tua proposta è stata accettata</span>
                        <?php
                    }elseif(intval($offer->ended)=== 1){
                        ?>
                        <span id = "alerttext">Complimenti, Hai portato a termine questa proposta</span>
                        <?php
                    }elseif(intval($offer->accepted) === -1 || $request->expiry < date("Y-m-d")){
                        ?>
                        <span id = "alerttext">Proposta scaduta</span>
                        <?php
                    }
                }else{
                    $date = date("Y-m-d");
                ?>
                    <button id="submit">ci penso io</button>
                    <div id="invisible" class="invisible">
                       <input type='date'min ="<?=$date?>" max="<?=$request->expiry?>" id = 'availability'>
                    </div>
                <?php

                }
            }else{
              ?>
                <span id = "alerttext">E' una tua richiesta </span>
                <?php
            }
        }else{
            header("Location: errorpage.php?error=2");
        }
        ?>
</div>