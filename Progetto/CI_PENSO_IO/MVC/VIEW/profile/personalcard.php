<?php
if(!isset($user) || !isset($personalcard)){ //if isset personalcard, the page if want to show personanlcard.php is "PROFILEPAGE"
?>
<div id= "wrapper" class="wrapper invisible">
    
<?php
}else{
?>
<div class="wrapper">  
<?php
}
?>
  <div class="container">
      
    <div class="profile-card">
        <?php
        if( (!isset($user) || !isset($personalcard)) && isset($special) ){ //special is setted from all pages  that want to call
                                                                           // profilecard (they are not profilepage.php).
        ?>
        <div id="close"><a href="">✖</a></div>
        
      <div class="img-container2">
          <?php
        }else{
        ?>
          <div class="img-container">
          <?php
        }
          ?>
        <img src="img/man.png" alt="">
      </div>
        
      <div id ="username" class="role">
          <?php
            if(isset($user) && !isset($special)){
          ?>
        <?= $user->username ?>
          <?php
          }
          ?>
      </div>
      
    </div>
    <div class="data">
      <div class="inner-data">
        <div class="data-content">
        <p>CPI</p>
        <span id="cpi">
          <?php
            if(isset($personalcard) && !isset($special)){
          ?>
            <?=$personalcard->cpi?>
            
            <?php
          }
          ?>
        </span>
      </div>
      <div class="data-content">
        <p>servizi offerti</p>
        <span id="n_contributions">
          <?php
            if(isset($personalcard) && !isset($special)){
          ?>
            <?=$personalcard->n_contributions?>
            
            <?php
          }
            ?>
        </span>
      </div>
      <div class="data-content">
        <p>voto</p>
        <span id ="rating">
            <?php
            if(!isset($special) && isset($personalcard) && $personalcard->n_rate > 0 ){
                print (number_format(($personalcard->rating / $personalcard->n_rate),1)."/5");
            }else{
                print 0;
            }
            ?>
            
        </span>
      </div>
      </div>
    </div>
   
    <div class="quote">
         
      <span id="firstname">
          <?php
            if(isset($user) && !isset($special)){
          ?>
            <?=$user->firstname?> <?=$user->lastname?>
          <?php
            }
          ?>
      </span>
      <p id="email">
          <?php
            if(isset($user) && !isset($special) ){
          ?>
          Email: <?= $user->email ?>
          <?php
            } 
          ?>
      </p>
      <p id="phone">
          <?php
            if(isset($user) && !isset($special)){
          ?>
            Numero : <?= $user->phone ?>
          <?php
            }
          ?>
      </p>
      <p id="study">
          <?php
            if(isset($personalcard) && !isset($special)){
          ?>
          Professione: <?=$personalcard->study?>
          <?php
            }
          ?>
      </p>
      
          <?php
          if(isset($special)){
            require_once 'MVC/VIEW/profile/star_rating.php';
          }
          ?>
          
     
    </div>
  </div>
</div>