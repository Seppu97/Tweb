<div id="rate" class="rate invisible">
    <input type="radio" id="star5" name="rate" value="5" class="star_clicked"/>
    <label for="star5" title="text">5 stars</label>
    <input type="radio" id="star4" name="rate" value="4" class="star_clicked" />
    <label for="star4" title="text">4 stars</label>
    <input type="radio" id="star3" name="rate" value="3" class="star_clicked" />
    <label for="star3" title="text">3 stars</label>
    <input type="radio" id="star2" name="rate" value="2" class="star_clicked" />
    <label for="star2" title="text">2 stars</label>
    <input type="radio" id="star1" name="rate" value="1" class="star_clicked" />
    <label for="star1" title="text">1 star</label>
    <button id="vote">Vota </button>
</div>
