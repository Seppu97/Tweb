<div class="topnav" id="myTopnav">
  <a href="index.php" class="active">Home</a>
  <div class="dropdown">
    <button class="dropbtn">Mie Richieste 
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="personalrequestpage?mode=1">Nuove</a>
      <a href="personalrequestpage?mode=2">In Corso</a>
      <a href="personalrequestpage?mode=3">Terminate</a>
    </div>
  </div> 
  
    <div class="dropdown">
    <button class="dropbtn">Mie Proposte 
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="personalofferpage.php?mode=1">Nuove</a>
      <a href="personalofferpage.php?mode=2">Accettate</a>
      <a href="personalofferpage.php?mode=3">Effettuate</a>
      <a href="personalofferpage.php?mode=4">Scadute o Rifiutate</a>
    </div>
  </div>
  
  <div id="dropdownprofile" class="dropdown" onmouseover="changeIcon()" onmouseout="resetIcon()">
        <button id = "dropbtnimg" class="dropbtn"> <img id="profileimage" src="img/profile.png" height="24" width="24" alt="profile">
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="profilepage.php">Profilo</a>
      <a href="logout.php">Logout</a>
    </div>
  </div>
  
  <a href="javascript:void(0);" class="icon" onclick="myFunction()">&#9776;</a>
</div>

<!-- script inserted here for simplify my code, because i added this function later and it's an experiment
    morover, w3school suggested me to insert this code in html page because it's very very short-->
<script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}

function changeIcon(event){
    $("profileimage").src="img/profile2.png";
}

function resetIcon(event){
    $("profileimage").src="img/profile.png";
}
</script>
