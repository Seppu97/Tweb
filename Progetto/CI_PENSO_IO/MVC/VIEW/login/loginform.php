
<form class="login" action="login.php" method="post">
    <h1 class="login-title">Benvenuto</h1>
    <input type="text" class="login-input" placeholder="Username" autofocus name="username">
    <input type="password" class="login-input" placeholder="Password" name="password">
    <input type="submit" value="Sign In" class="login-button">
    <p class="login-lost"><a href="register.php">Register</a></p>
</form>