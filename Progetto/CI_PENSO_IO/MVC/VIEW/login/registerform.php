<!--<h1 id='notice' style='text-align:center'></h1>-->
<form id="registerform" class="login" >
   <h1 class="login-title">Registrati</h1>
   <input type="text" class="login-input" placeholder="Username" autofocus name="username" required="required" maxlength="20" id="user">
    
    <input type="password" class="login-input" placeholder="Password" name="password"
           required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" 
           title="La password deve contenere una lettera maiuscola,un carattere speciale e un numero ed essere lunga 8 caratteri">
    
    <input type="text" class="login-input" placeholder="Nome" name="firstname" required="required">
    <input type="text" class="login-input" placeholder="Cognome" name="lastname" required="required">
    <input type="email" class="login-input" placeholder="Email" name="email" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" id="email">
    <input type="text" class="login-input" placeholder="Data di nascita" name="birthday" required="required" onfocus="(this.type='date',this.max='<?=$date?>')">
    <input type="text" class="login-input" placeholder="Cellulare" name="phone" required="required" minlength="10" maxlength="10">
    <input type="text" class="login-input" placeholder="Carta d'identià" name="identitycard" required="required" maxlength="9" id="identitycard">
    <input type="button" value="Registrati" class="login-button" id="registrati">
    <p class="login-lost">Hai già un account ? <a href="loginpage.php">Login</a></p>

</form>

