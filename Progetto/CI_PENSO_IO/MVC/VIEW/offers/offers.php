<h1>Proposte Ricevute</h1>
<?php
foreach($offers as $offer){
    if(intval($offer->accepted) !== -1 ){ //refuse or expired proposes are not shown
        $userid = $users[$offer->users]->id;
        $username = $users[$offer->users]->username;
        $availability = $offer->availability;
        $rating = $personalcard[$offer->users]->rating;
        $accepted = $offer->accepted;
        $offerid = $offer->id;
?>
<div class="box" data-value="<?= $offerid ?>">
    <img class="categoryimg" src="img/man.png" alt="category" />
    <a href="#"><span class="username" data-value="<?= $userid ?>"><?= $username ?><img src="img/personalcard.png" width="21" height="21" alt="personalcard"></span></a>
    <div class ="middle"><span class="data">disponibilità : </span> <?= $availability ?></div>
    <div class="star">
       <?= $rating ?>
    </div>
    <?php
    if(intval($accepted) === 0){
    ?>
        <input class="confirmbutton" type="image" src="img/confirm.png"  alt="confirm">
    <?php
    }
    ?>
</div>
<?php
    }
}
?>
