<?php
require_once 'MVC/MODEL/model.php';
class Controller{
    
    public function invoke($username = NULL){
        require_once 'MVC/VIEW/profile/menu.php';
        
        $model = new Model();
        
        if($username == NULL){
            $user = $model->getUser($_SESSION['username']);
        }else{
            $user = $model->getUser($username);
        }
        $personalcard = $model->getPersonalcard($user->personalcard);
        require_once 'MVC/VIEW/profile/personalcard.php';
        
    }
}
?>

