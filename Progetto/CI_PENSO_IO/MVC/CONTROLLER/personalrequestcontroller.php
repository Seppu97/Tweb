<?php
require_once 'MVC/MODEL/model.php';

    class Controller{
        public function invokeDoneRequests($userid){
            $model = new Model();
            $user = $model->getUserById($userid); // user exist ( is in session)
            if($user){
                    $requests = $model->getDoneRequests($user->id); // but it could not has any requests.
                    if($requests !== FALSE){
                    $i = 0 ;
                    if($requests != null){ //if exist one request or more
                        foreach($requests as $request){
                            $services[$i] = $model->getService($request->service);
                            $i++;
                        }
                        $personal = true;// peronal requests (contains link to all proposes)
                        require_once 'MVC/VIEW/request/request.php';
                    }
                }else{
                    header("Location: errorpage.php?error=2");
                }
            }else{  
                header("Location: errorpage.php?error=2");
            }
        }
        
        public function invokeProgressRequests($userid){
            
            $model = new Model();
            $user = $model->getUserById($userid);
            if($user){
                $requests = $model->getProgressRequests($user->id);
                if($requests !== FALSE){ 
                    $i = 0 ;
                    if($requests != null){
                        foreach($requests as $request){
                            $services[$i] = $model->getService($request->service);
                            $i++;
                        }
                        $personal = true;// peronal requests (contains link to all proposes)
                        require_once 'MVC/VIEW/request/request.php';
                    }
                }else{
                    header("Location: errorpage.php?error=2");
                }
            }else{
                header("Location: errorpage.php?error=2");
            }
            
        }
        
        
        public function invokeNewRequests($userid){
            $model = new Model();
            $localdate = date("Y-m-d");
            $user = $model->getUserById($userid);
            if($user){
                
                $requests = $model->getNewRequestsInExpiry($user->id, $localdate);
               
                if($requests !== FALSE){
                    $i = 0 ;
                    if($requests !== NULL){
                        foreach($requests as $request){
                            $services[$i] = $model->getService($request->service);
                            $i++;
                        }
                        $personal = true;// peronal requests (contains link to all proposes)
                        require_once 'MVC/VIEW/request/request.php';
                    }
                }else{
                   header("Location: errorpage.php?error=2");
               }
            }else{
                header("Location: errorpage.php?error=2");
            }
        }
        
    }
?>