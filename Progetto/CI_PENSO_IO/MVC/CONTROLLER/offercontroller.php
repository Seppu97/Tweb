<?php
require_once 'MVC/MODEL/model.php';
class Controller{
    public function invoke($requestid){ //request id of logged user
        
        $model = new Model();
        $localdate = date("Y-m-d");
        $offers = $model->getOffersByRequestInExpiry($requestid,$localdate);
        if($offers){
            foreach($offers as $offer){
                $users[$offer->users] = $model -> getUserById($offer->users);
                $personalcard[$offer->users] = $model->getPersonalcard($users[$offer->users]->personalcard);
            }
            $special = TRUE;//used to inform personalcard.php that it's NOT called from profilepage.php but by another page.
                //in this way it not do query on db directly , but these query are done by AJAX request to fill in fields.
            require_once 'MVC/VIEW/profile/personalcard.php';
            require_once 'MVC/VIEW/offers/modal.php';
            require_once'MVC/VIEW/offers/offers.php';
            
        }elseif($offers === FALSE){
            header("Location: errorpage.php?error=2");
        }
    }
    
}
?>