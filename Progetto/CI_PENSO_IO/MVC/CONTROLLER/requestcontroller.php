<?php
require_once 'MVC/MODEL/model.php';
    class Controller{
        public function invoke(){
            $model = new Model();
            $localdate = date("Y-m-d");
            $requests = $model->getRequestsInExpiry($localdate);
            $i = 0 ;
            if($requests){ // of course ,if exist a $request, also exist that service and that user
                foreach($requests as $request){
                    $services[$i] = $model->getService($request->service);
                    $users[$i] = $model->getUserById($request->user); 
                    $i++;
                }
                
                $special = TRUE; //used to inform personalcard.php that it's NOT called from profilepage.php but by another page.
                //in this way it not do query on db directly , but these query are done by AJAX request to fill in fields.
                require_once 'MVC/VIEW/profile/personalcard.php';
                require_once 'MVC/VIEW/request/request.php';
            }
        }
    }
?>

