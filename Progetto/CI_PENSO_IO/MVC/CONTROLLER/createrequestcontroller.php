<?php
require_once 'MVC/MODEL/model.php';
class Controller{
    public function invoke(){
        $model = new Model();
        $services = $model->getAllServices(); //used for category menu
        if($services){
            require_once 'MVC/VIEW/request/createrequest.php';
        }else{
            header("Location: errorpage.php?error=2");
           
        }
    }
}
?>