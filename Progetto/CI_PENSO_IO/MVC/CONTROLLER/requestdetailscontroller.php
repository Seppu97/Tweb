<?php
require_once 'MVC/MODEL/model.php';

class Controller{
    public function invoke($requestid){
        $model = new Model();
        $request = $model->getRequest($requestid);
        if($request){ //i check if exist this request ( i also prevent bad parameter in url)
            $service = $model->getService($request->service);//service information of request.
            $user = $model->getUserById($request->user);//get user of request.
            //saving data used to register the user's offer.
            $_SESSION['expiry'] = $request->expiry;
            $_SESSION['request'] = $request->id;
            require_once 'MVC/VIEW/request/requestdetails.php';
        }elseif($request === NULL){
            header("Location: errorpage.php?error=1");
        }else{
             header("Location: errorpage.php?error=2");
        }
    }
}
?>
