<?php
require_once 'MVC/MODEL/model.php';

class Controller{
    public function invoke($userid,$mode){
        $model = new Model();
        
        $user = $model->getUserById($userid);
        
        if($user){
            if($mode === 1){
                print "<h1>Le tue nuove proposte (ancora non viste)</h1>";
                $offers = $model->getNewOffersByUser($userid,date("Y-m-d"));
            }elseif($mode === 2){
                print "<h1>Proposte accettate</h1>";
                $offers = $model->getAcceptedOffersByUser($userid, date("Y-m-d"));
            }elseif($mode === 3){
                print "<h1>Proposte portate a termine</h1>";
                $offers = $model->getDoneOffersByUser($userid);
            }elseif($mode === 4){
                print "<h1>Proposte scadute o rifiutate</h1>";
                $offers = $model->getOverdueOffersByUser($userid, date("Y-m-d"));
            }else{
                header("Location: errorpage.php?error=1");
            }
            if($offers !== FALSE){
                $i=0;
                if($offers !== NULL){
                    foreach($offers as $offer){
                        $requests[$i] = $model->getRequest($offer->request);
                        $services[$i] = $model->getService($requests[$i]->service);
                        $users[$i] = $model->getUserById($requests[$i]->user);
                        $i++;
                    }
                     $personaloffers = TRUE;
                     $special = TRUE;
                     require_once 'MVC/VIEW/profile/personalcard.php';
                    require_once 'MVC/VIEW/request/request.php';
                }
            }else{
                header("Location: errorpage.php?error=2");
            }
        }else{
            header("Location: errorpage.php?error=2");
        }
        
    }
}
?>

