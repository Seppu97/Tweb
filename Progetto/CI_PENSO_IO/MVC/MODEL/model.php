<?php
require_once 'MVC/MODEL/classes/User.php';
require_once 'MVC/MODEL/classes/Personalcard.php';
require_once 'MVC/MODEL/classes/Request.php';
require_once 'MVC/MODEL/classes/Service.php';
require_once 'MVC/MODEL/classes/Offer.php';
require_once 'MVC/MODEL/classes/Vote.php';
require_once 'MVC/MODEL/function_db/query.php';

class model{
    public function getUser($username){
        $user = NULL;
        $rows =  userQuery($username);
        if($rows){
            foreach ($rows as $row){
                $user =  new User($row['id'],$row["identitycard"],$row["username"],$row["password"],$row["firstname"],$row["lastname"]
                        ,$row["birthday"],$row["email"],$row["phone"],$row["personalcard"]);
            }
        }elseif($rows === FALSE){
            $user = FALSE;
        }
        
        return $user;
    }
    
    public function getUserById($id){
        $user = NULL;
        try{
            $rows = userIdQuery($id);
        }catch(PDOException $e){
            $user = FALSE;
            
        }
        if($rows){
            foreach ($rows as $row){
                $user  =  new User($row['id'],$row["identitycard"],$row["username"],$row["password"],$row["firstname"],$row["lastname"]
                        ,$row["birthday"],$row["email"],$row["phone"],$row["personalcard"]);
            }
        }
        return $user;
    }
    
    public function getUsers(){
        $rows = allUsersQuery();

        foreach($rows as $row){
            $user[$row['username']]= new User($row['id'],$row["identitycard"],$row["username"],$row["password"],$row["firstname"],$row["lastname"]
                    ,$row["birthday"],$row["email"],$row["phone"],$row["personalcard"]);
        }
        
        return $user;
    }
    
    public function getPersonalcard($cardid){
       
        $rows = personalCardQuery($cardid);
        
        foreach ($rows as $row){
            $personalcard[$row['id']] = new Personalcard($row['id'], $row['study'], $row['cpi'], $row['rating'],
                    $row['n_rate'], $row['n_contributions']);
        }
        
        return $personalcard[$cardid];
    }
    
    public function updatePersonalcard($personalcard){
       $update = updatePersonalCard($personalcard);
       return $update;
    }
    
    public function insertUser($user){
        $response = insertUserQuery($user);
        
        return $response;
    }

    public function getRequests(){
        $rows = allRequestsQuery();
        $request = NULL;
        
        foreach($rows as $row){
            $request[$row['id']] = new Request($row['id'], $row['title'], $row['description'], $row['date_request'], 
                    $row['expiry'], $row['ended'],$row['accepted'], $row['user'], $row['service']);
        }
        
        return $request;
    }
    
    public function getRequest($requestid){
        
        $rows = requestQuery($requestid);
        $request=NULL;
        
        if($rows!==FALSE){
            foreach($rows as $row){
                $request[$row['id']] = new Request($row['id'], $row['title'], $row['description'], $row['date_request'], 
                        $row['expiry'], $row['ended'],$row['accepted'], $row['user'], $row['service']);
            }
        }else{
            return FALSE;
        }
        if($request){
            return $request[$requestid];
        }else{
            return NULL;
        }
    }
    
    public function getDoneRequests($userid){
        $request = NULL;
         $rows = DoneRequestsByUser($userid);
         if($rows === FALSE){
             $request = FALSE;
         }
        if($rows){
            foreach($rows as $row){
                $request[$row['id']] = new Request($row['id'], $row['title'], $row['description'], $row['date_request'], 
                        $row['expiry'], $row['ended'],$row['accepted'], $row['user'], $row['service']);
            }
        }
        
        return $request;
    }
   
    public function getProgressRequests($userid){
        $request = NULL;
        $rows = ProgressRequestsByUser($userid);
        if($rows === FALSE){
            $request = FALSE;
        }
        if($rows){
            foreach($rows as $row){
                $request[$row['id']] = new Request($row['id'], $row['title'], $row['description'], $row['date_request'], 
                        $row['expiry'], $row['ended'],$row['accepted'], $row['user'], $row['service']);
            }
        }
        return $request;
    }
    
    public function getNewRequests($userid){
        $rows = NewRequestsByUser($userid);
        $request = NULL;
        
        foreach($rows as $row){
            $request[$row['id']] = new Request($row['id'], $row['title'], $row['description'], $row['date_request'], 
                    $row['expiry'], $row['ended'],$row['accepted'], $row['user'], $row['service']);
        }
        
        return $request;
    }
    
    public function getNewRequestsInExpiry($userid,$localdate){
        $request = NULL;
        
        $rows = NewRequestsbyUserInExpiry($userid, $localdate);
        if($rows === FALSE){
            return FALSE;
        }
        if($rows){
            foreach($rows as $row){
                $request[$row['id']] = new Request($row['id'], $row['title'], $row['description'], $row['date_request'], 
                        $row['expiry'], $row['ended'],$row['accepted'], $row['user'], $row['service']);
            }
        }
        return $request;
    }
    
    public function getRequestsInExpiry($localdate){
        $rows = RequestsInExpiry($localdate);
        $request = null;
        
        if($rows){
            foreach($rows as $row){
                $request[$row['id']] = new Request($row['id'], $row['title'], $row['description'], $row['date_request'], 
                        $row['expiry'], $row['ended'],$row['accepted'], $row['user'], $row['service']);
            }
        }elseif ($rows === FALSE){
            $request = FALSE;
        }
        return $request;
    }
    
    public function insertRequest($request){
        try{
            insertRequest($request);
            $returnvalue = 1;
        }catch(PDOExecption $e) { 
             $returnvalue = FALSE;
        }
        
        return $returnvalue;
    }
    
    public function getService($serviceid){
         $rows = serviceQuery($serviceid);
        
        foreach($rows as $row){
            $service[$row['id']] = new Service($row['id'], $row['category_name'], $row['cpi']);
        }
        
        return $service[$serviceid];
    }
    
    public function getAllServices(){
        $rows = allServices();
        $services = NULL;
        
        foreach($rows as $row){
            $services[$row['id']] = new Service($row['id'], $row['category_name'], $row['cpi']);
        }
        
        return $services;
    }
    
    public function getOffer($offerid){
        $rows = offer($offerid);
        
        if($rows){
            foreach($rows as $row){
                $offer = new Offer($row['id'],$row['availability'],$row['date_offers'],$row['ended'],$row['accepted']
                    ,$row['users'],$row['request']);
            }
        }else{
            $offer = FALSE;
        }
        
        return $offer;
    }
    
    public function getOfferByUserRequest($requestid,$userid){
        $offer = NULL;
        $rows = offerByRequestAndUser($requestid, $userid);
        if($rows){
            foreach ($rows as $row){
                $offer = new Offer($row['id'],$row['availability'],$row['date_offers'],$row['ended'],$row['accepted']
                        ,$row['users'],$row['request']);
            }
        }elseif($rows === FALSE){
            $offer = FALSE;
        }
        return $offer;
    }
    
    //get all offers for a the specific request, also expired offers.
    public function getOffersByRequest($requestid){
        $rows = offersByRequest($requestid);
        $offer = NULL;
        
        foreach ($rows as $row){
            $offer[$row['id']] = new Offer($row['id'],$row['availability'],$row['date_offers'],$row['ended'],$row['accepted']
                    ,$row['users'],$row['request']);
        }
        
        return $offer;
    }
    
    //get all offers for the specific request (NOT EXPIRED)
    public function getOffersByRequestInExpiry($requestid,$localdate){
        $offer = NULL;
        $rows = offersByRequestInExpiry($requestid, $localdate);
        
        if($rows === FALSE){
            $offer = FALSE;
        }
        if($rows){
            foreach ($rows as $row){
                $offer[$row['id']] = new Offer($row['id'],$row['availability'],$row['date_offers'],$row['ended'],$row['accepted']
                        ,$row['users'],$row['request']);
            }
        }
        return $offer;
    }
    
    public function insertOffer($offer){
        $insert = insertOfferQuery($offer);
        if($insert){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    /*accepting an offer consist in some steps : -get the offer , set its accepted field to 1 , -get the request of such offer and 
     set its accepted field to 1,-to reject al the others offers to that request ( set its accepted field to -1).*/
    public function acceptOffer($offerid){
        $offer = $this->getOffer($offerid);
        $updateOffer = FALSE;
        $updateRequest = FALSE;
        
        if($offer && intval($offer->ended) === 0 && intval($offer->accepted) === 0){
            $request = $this->getRequest($offer->request);
            $loggedUserId = $this->getUser($_SESSION['username'])->id;// can replace with $_SESSION['userd']
           
            if($request && intval($request->accepted) === 0 && intval($request->ended) === 0 && $request->user === $loggedUserId){
                $offer->accepted = 1;
                $request->accepted = 1;
                $updateOffer = updateOffer($offer);
                $updateRequest = updateRequest($request);
                
                $offers_reject = $this->getOffersByRequest($request->id);
                foreach($offers_reject as $offer_reject){
                    if($offer_reject->id !== $offer->id){
                        $offer_reject->accepted = -1;
                        $updateOfferReject = updateOffer($offer_reject);
                        if(!$updateOfferReject){
                            return FALSE;
                        }
                    }
                }
            }
       }
         
         return ($updateOffer && $updateRequest);
    }
    
    public function getNewOffersByUser($userid,$localdate){
        $offer = NULL;
        $rows = newOffersByUser($userid, $localdate);
        
        if($rows === FALSE){
            $offer = FALSE;
        }
        if($rows){
            foreach ($rows as $row){
                $offer[$row['id']] = new Offer($row['id'],$row['availability'],$row['date_offers'],$row['ended'],$row['accepted']
                        ,$row['users'],$row['request']);
            }
        }
        return $offer;
    }
    
    public function getAcceptedOffersByUser($userid,$localdate){
        $offer = NULL;
        $rows = acceptedOffersByUser($userid, $localdate);
        
        if($rows === FALSE){
            $offer = FALSE;
        }
        if($rows){
            foreach ($rows as $row){
                $offer[$row['id']] = new Offer($row['id'],$row['availability'],$row['date_offers'],$row['ended'],$row['accepted']
                        ,$row['users'],$row['request']);
            }
        }
        return $offer;
    }
    
    public function getDoneOffersByUser($userid){
        $offer = NULL;
        $rows = doneOffersByUser($userid);
        
        if($rows === FALSE){
            $offer = FALSE;
        }
        if($rows){
            foreach ($rows as $row){
                $offer[$row['id']] = new Offer($row['id'],$row['availability'],$row['date_offers'],$row['ended'],$row['accepted']
                        ,$row['users'],$row['request']);
            }
        }
        return $offer;
    }
    
    public function getOverdueOffersByUser($userid,$localdate){
        $offer = NULL;
        $rows = overdueOffersByUser($userid, $localdate);
        
        if($rows === FALSE){
            $offer = FALSE;
        }
        if($rows){
            foreach ($rows as $row){
                $offer[$row['id']] = new Offer($row['id'],$row['availability'],$row['date_offers'],$row['ended'],$row['accepted']
                        ,$row['users'],$row['request']);
            }
        }
        return $offer;
    }
    
   public function updateOffer($offer){
       $update = updateOffer($offer);
       
       if($update){
           return TRUE;
       }else{
           return FALSE;
       }
   }
   
   //TRUE = user can vote , FALSE = internal error, NULL = user not done any offers, -1 = users already voted.
   public function canVote($voter,$voted){
       $rows = doneOffersByUserToAnother($voter, $voted);
       $rows2 = voteByUserToAnother($voter, $voted);
       $offers = NULL;
       $vote = NULL;
       
       if($rows && $rows2){
           foreach ($rows as $row){
               $offers[$row['id']] =  new Offer($row['id'],$row['availability'],$row['date_offers'],$row['ended'],$row['accepted']
                        ,$row['users'],$row['request']);
           }
           
           foreach ($rows2 as $row2){
               $vote = $row2['id'];
           }
       }elseif($rows === FALSE || $rows2 === FALSE){
           return FALSE;
       }
       
       if($offers !== NULL && $vote === NULL){ 
           return TRUE;
       }elseif ($vote != NULL){
           return -1;
       }else{
           return NULL;
       }
   }
   
   public function insertVote($voter,$voted,$rate){
       $vote = new Vote($voter, $voted, $rate);
       $insert = insertVote($vote);
       $user = $this->getUserById($voted);
      
       $cardid= $user->personalcard;
       
       if($insert && $user && $cardid){
           $personalcard = $this->getPersonalcard($cardid);
           $personalcard->n_rate++;
           $personalcard->rating += $rate;
           $response = updatePersonalCard($personalcard);
       }else{
           $response = FALSE;
       }
       
       return $response;
   }
}
?>