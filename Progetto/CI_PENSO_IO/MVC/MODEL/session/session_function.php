<?php
require_once 'MVC/MODEL/model.php';

function is_password_correct($username,$password) {
    
    $model = new Model();
    $user = $model->getUser($username);
    
    if($user){
        return password_verify($password,$user->password);
    }else{
        return FALSE;
    }
}

function check_logged_in() {
    
    if (!isset($_SESSION["username"])) {
       redirect("loginpage.php");
    }
}

function check_to_index(){
    if(isset($_SESSION['username'])){
        redirect('index.php');
    }
}

function redirect($url, $flash_message = NULL,$refreshtime = 0) {
    
    if ($flash_message) {
        $_SESSION["flash"] = $flash_message;
    }
    if($refreshtime == 0){
        header("Location: $url");
    }else{
        header("Refresh:$refreshtime;url='$url'");
    }
    die;
}
?>
