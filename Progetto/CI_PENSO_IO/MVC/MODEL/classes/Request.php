<?php

class Request {
    public $id;
    public $title;
    public $description;
    public $date_request;
    public $expiry;
    public $ended;
    public $accepted;
    public $user; //single user
    public $service;
    
    function __construct($id, $title, $description, $date_request, $expiry, $ended,$accepted, $user, $service) {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->date_request = $date_request;
        $this->expiry = $expiry;
        $this->ended = $ended;
        $this->accepted = $accepted;
        $this->user = $user;
        $this->service = $service;
    }

}
