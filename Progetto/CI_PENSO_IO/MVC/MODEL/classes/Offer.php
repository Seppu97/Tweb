<?php


class Offer {
    public $id;
    public $availability;
    public $date_offers;
    public $ended;
    public $accepted;
    public $users;
    public $request;
    
    function __construct($id, $availability, $date_offers, $ended, $accepted, $users, $request) {
        $this->id = $id;
        $this->availability = $availability;
        $this->date_offers = $date_offers;
        $this->ended = $ended;
        $this->accepted = $accepted;
        $this->users = $users;
        $this->request = $request;
    }

}
