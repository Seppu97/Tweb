<?php
class Personalcard{
    public $id;
    public $study;
    public $cpi;
    public $rating;
    public $n_rate;
    public $n_contributions;
    
    function __construct($id, $study, $cpi, $rating, $n_rate, $n_contributions) {
        $this->id = $id;
        $this->study = $study;
        $this->cpi = $cpi;
        $this->rating = $rating;
        $this->n_rate = $n_rate;
        $this->n_contributions = $n_contributions;
    }

    
}

