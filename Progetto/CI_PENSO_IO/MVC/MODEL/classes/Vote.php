<?php
class Vote{
    public $voter;
    public $voted;
    public $vote;
    
    function __construct($voter, $voted, $vote) {
        $this->voter = $voter;
        $this->voted = $voted;
        $this->vote = $vote;
    }

}
?>