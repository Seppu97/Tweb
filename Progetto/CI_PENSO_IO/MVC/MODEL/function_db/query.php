<?php

require_once 'MVC/MODEL/function_db/connect.php';
try{ //connection may fail
    $db = connect();
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
    $db->exec("set names utf8");
} catch (PDOException $e){
    header("Location: errorpage.php?error=2");
}

// METHODS OF USER:
//return all users 
function allUsersQuery() {
    global $db;

    $users = $db->query("SELECT * FROM users");
    return $users;
}

//return user by username
function userQuery($username) {
    global $db;
    $name = $db->quote($username);
    try {
        $user = $db->query("SELECT * FROM users WHERE username like $name");
    } catch (PDOException $e) {
        $user = FALSE;
    }
    return $user;
}

//return user by ID
function userIdQuery($id) {
    global $db;

    $user = $db->query("SELECT * FROM users WHERE id = $id");

    return $user;
}

//return user by email
function userEmailQuery($email) {
    global $db;

    $email = $db->quote($email);
    $user = $db->query("SELECT * FROM users WHERE email like $email");

    return $user;
}

//return user by identitycard
function userIdentityQuery($identitycard) {
    global $db;

    $identitycard = $db->quote($identitycard);
    $user = $db->query("SELECT * FROM users WHERE identitycard like $identitycard");

    return $user;
}

//return two users by email and identitycard (only two because email and identitycard are unique)
function userIdentityEmailQuery($email, $identitycard) {
    global $db;

    $email = $db->quote($email);
    $identitycard = $db->quote($identitycard);

    $user = $db->query("SELECT * FROM users WHERE email LIKE $email OR identitycard LIKE $identitycard");

    return $user;
}

//return users by username,email and identiticard 
function userAllDataQuery($username, $email, $identitycard) {
    global $db;

    $username = $db->quote($username);
    $email = $db->quote($email);
    $identitycard = $db->quote($identitycard);

    $user = $db->query("SELECT * FROM users WHERE username LIKE $username OR email LIKE $email OR identitycard LIKE $identitycard");

    return $user;
}

//insert a new user in DB ( use the Object User)
function insertUserQuery($user) {
    global $db;

    try {
        $db->exec("INSERT INTO personalcard (cpi,rating,n_rate,n_contributions) VALUES (6,0,0,0)");
    } catch (PDOExecption $e) {
        return FALSE;
    }

    $id_personalcard = $db->lastInsertId();
    $username = $db->quote($user->username);
    $identitycard = $db->quote($user->identitycard);
    $firstname = $db->quote($user->firstname);
    $lastname = $db->quote($user->lastname);
    $password = $db->quote($user->password);
    $birthday = $db->quote($user->birthday);
    $email = $db->quote($user->email);
    $phone = $db->quote($user->phone);

    $query = "INSERT INTO users (identitycard,username,password,firstname,lastname,birthday,email,phone,personalcard) VALUES "
            . "($identitycard,$username,$password,$firstname,$lastname,$birthday,$email,$phone,$id_personalcard)";

    try {
        $db->exec($query);
        $response = TRUE;
    } catch (PDOExecption $e) {
        return FALSE;
    }

    if (isset($response)) {
        return $response;
    }
}

//METHODS OF PERSONALCARD:
//return personalcard by id ( you can find the id in users->personalcard
function personalCardQuery($cardid) {
    global $db;

    $card = $db->query("SELECT * FROM personalcard WHERE id = $cardid");

    return $card;
}

//update the data of a personalcard (accept the Object Personalcard)
function updatePersonalCard($personalcard) {
    global $db;

    $study = $db->quote($personalcard->study);
    
    try {
        $db->exec("UPDATE personalcard SET study = $study, cpi = $personalcard->cpi, rating = $personalcard->rating, "
                . "n_rate = $personalcard->n_rate, n_contributions = $personalcard->n_contributions WHERE id = $personalcard->id");
        $response = TRUE;
    } catch (PDOExecption $e) {
        $response = FALSE;
    }

    return $response;
}

//METHODS OF REQUEST:
//return a request by ID
function requestQuery($requestid) {
    global $db;

    try {
        $request = $db->query("SELECT * FROM request WHERE id = $requestid");
    } catch (PDOException $e) {
        $request = FALSE;
    }

    return $request;
}

//return all requests of DB ( only not ended and not accepted)
function allRequestsQuery() {
    global $db;

    $requests = $db->query("SELECT * FROM request WHERE ended = 0 AND accepted = 0 ORDER BY date_request DESC");
    return $requests;
}

//return New requests  of all users (not accepted and not ended) that has expiry >= localdate
function RequestsInExpiry($localdate) {
    global $db;

    $date = $db->quote($localdate);
    try{
        $requests = $db->query("SELECT * FROM request WHERE ended = 0 AND accepted = 0 AND expiry >= $date "
            . "ORDER BY date_request DESC");
    }catch(PDOException $e){
        $requests = FALSE;
    }
    return $requests;
}

//return all done requests (done = ended) of a user
function DoneRequestsByUser($userid) {
    global $db;

    try {
        $requests = $db->query("SELECT * FROM request WHERE (ended = 1 AND accepted = 1) AND user = $userid "
                . "ORDER BY date_request DESC");
    } catch (PDOException $e) {
        $requests = FALSE;
    }
    return $requests;
}

//return requests of an user for wich an offer has been accepted (waiting to complete it) 
function ProgressRequestsByUser($userid) {
    global $db;

    try {
        $requests = $db->query("SELECT * FROM request WHERE (ended = 0 AND accepted = 1) AND user = $userid "
                . "ORDER BY date_request DESC");
    } catch (PDOException $e) {
        return FALSE;
    }

    return $requests;
}

//return NEW requests posted from user ( not ended and not accepted)
function NewRequestsByUser($userid) {
    global $db;

    $requests = $db->query("SELECT * FROM request WHERE ended = 0 AND accepted = 0 AND user = $userid "
            . "ORDER BY date_request DESC");

    return $requests;
}

//return NEW requests posted from user BUT only those with expiry >= local day
function NewRequestsbyUserInExpiry($userid, $localdate) {
    global $db;

    $date = $db->quote($localdate);
    try {
        $requests = $db->query("SELECT * FROM request WHERE ended = 0 AND accepted = 0 AND user = $userid "
                . "AND expiry >= $date ORDER BY date_request DESC");
    } catch (PDOException $e) {
        $requests = FALSE;
    }
    return $requests;
}

//insert a new request in DB (accept an Object Request)
function insertRequest($request) {
    global $db;

    $title = $db->quote($request->title);
    $description = $db->quote($request->description);
    $date_request = $db->quote($request->date_request);
    $expiry = $db->quote($request->expiry);
    $serviceid = $request->service;
    $userid = $request->user;


    $db->exec("INSERT INTO request (title,description,date_request,expiry,service,user) "
            . "VALUES ($title,$description,$date_request,$expiry,$serviceid,$userid)");
}

//return a service by ID
function serviceQuery($serviceid) {
    global $db;

    $service = $db->query("SELECT * FROM service WHERE id = $serviceid");

    return $service;
}

//return all services of DB
function allServices() {
    global $db;

    $services = $db->query("SELECT * FROM service");

    return $services;
}

//return an offer by ID
function offer($offerid) {
    global $db;

    try {
        $offer = $db->query("SELECT * FROM offers WHERE id = $offerid ");
    } catch (PDOException $e) {
        $offer = FALSE;
    }

    return $offer;
}

//return the offer done by user for a specific request
function offerByRequestAndUser($requestid, $userid) {
    global $db;

    try {
        $offer = $db->query("SELECT * FROM offers WHERE users = $userid AND request = $requestid ");
    } catch (PDOException $e) {
        $offer = FALSE;
    }
    return $offer;
}

//return all the offers for a specific request
function offersByRequest($requestid) {
    global $db;

    $offers = $db->query("SELECT * FROM offers WHERE request = $requestid ");

    return $offers;
}

//return all the offers for a specific request with availability >= local date
function offersByRequestInExpiry($requestid, $localdate) {
    global $db;

    $date = $db->quote($localdate);
    try {
        $offers = $db->query("SELECT * FROM offers WHERE request = $requestid AND availability >= $date ");
    } catch (PDOException $e) {
        return FALSE;
    }
    return $offers;
}

//return all new offers don by user (not accepted and not ended of course)
function newOffersByUser($userid, $localdate) {
    global $db;

    $date = $db->quote($localdate);
    try {
        $offers = $db->query("SELECT * FROM offers WHERE users = $userid AND ended = 0 AND accepted = 0 AND availability >= $date ");
    } catch (PDOException $e) {
        return FALSE;
    }
    return $offers;
}

// get the accepted offers. if  availability date is before local date and the ended is equal to 0 , user not done the service
//and the offer can't be showed in this section).
function acceptedOffersByUser($userid, $localdate) {
    global $db;

    $date = $db->quote($localdate);
    try {
        $offers = $db->query("SELECT * FROM offers WHERE users = $userid AND ended = 0 AND accepted = 1 AND availability >= $date ");
    } catch (PDOException $e) {
        return FALSE;
    }
    return $offers;
}

//get the offers that user has done correctly!(good work)
function doneOffersByUser($userid) {
    global $db;

    try {
        $offers = $db->query("SELECT * FROM offers WHERE users = $userid AND ended = 1 AND accepted = 1 ");
    } catch (PDOException $e) {
        return FALSE;
    }
    return $offers;
}

//return the Overdue or not accepted offers of a user (not accepted is if has been accepted another offer
function overdueOffersByUser($userid, $localdate) {
    global $db;

    $date = $db->quote($localdate);
    try {
        $offers = $db->query("SELECT * FROM offers WHERE users = $userid AND ended = 0 AND (availability < $date OR accepted = -1) ");
    } catch (PDOException $e) {
        return FALSE;
    }
    return $offers;
}

//insert a new offer made by user ( accepte an Object Offer)
function insertOfferQuery($offer) {
    global $db;

    $availability = $db->quote($offer->availability);

    $date_offers = $db->quote($offer->date_offers);

    try {
        $db->exec("INSERT INTO offers (availability,date_offers,users,request) VALUES"
                . " ($availability,$date_offers,$offer->users,$offer->request)");
        return TRUE;
    } catch (PDOExecption $e) {
        return FALSE;
    }
}

//update an existing Offer ( accept an Object having unchanged ID and new fields)
function updateOffer($offer) {
    global $db;

    $availability = $db->quote($offer->availability);
    $ended = $offer->ended;
    $accepted = $offer->accepted;
    $offerid = $offer->id;

    try {
        $db->exec("UPDATE offers SET availability = $availability, ended=$ended, accepted=$accepted WHERE id=$offerid");
        return TRUE;
    } catch (PDOExecption $e) {
        return FALSE;
    }
}

//update an existing request ( accept an Object having unchanged ID and new fields)
function updateRequest($request) {
    global $db;


    $ended = $request->ended;
    $accepted = $request->accepted;
    $requestid = $request->id;

    try {
        $db->exec("UPDATE request SET ended=$ended, accepted=$accepted WHERE id=$requestid");
        return TRUE;
    } catch (PDOExecption $e) {
        return FALSE;
    }
}

//return all offers done by voted user to voter user's requests.
function doneOffersByUserToAnother($voter, $voted) {
    global $db;
    $offers = NULL;
    try {
        $offers = $db->query("select offers.id,availability,date_offers,offers.ended,offers.accepted,offers.users,offers.request
                                  from request join offers on request.id = offers.request and request.user = $voter 
                                  where offers.users = $voted and offers.ended = 1");
    } catch (PDOException $e) {
        $offers = FALSE;
    }

    return $offers;
}

//return the vote done by an user to another user
function voteByUserToAnother($userid_one, $userid_two) {
    global $db;
    $vote = NULL;
    try {
        $vote = $db->query("select id from vote where voter = $userid_one and voted = $userid_two");
    } catch (PDOException $ex) {
        $vote = FALSE;
    }

    return $vote;
}

//insert a new vot( accept Object Vote)
function insertVote($vote) {
    global $db;

    try {
        $db->exec("INSERT INTO vote (voter,voted,vote) VALUES ($vote->voter,$vote->voted,$vote->vote)");
        $response = TRUE;
    } catch (PDOExecption $e) {
        $response = FALSE;
    }

    return $response;
}

?>
