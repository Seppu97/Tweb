<?php
session_start();
require_once'MVC/MODEL/session/session_function.php';
$username = filter_input(INPUT_POST,'username',FILTER_SANITIZE_STRING);
$password = filter_input(INPUT_POST,'password',FILTER_SANITIZE_STRING);

if ($username && $password) {
    
    if (is_password_correct($username,$password)) {
        if (isset($_SESSION)) {
            session_regenerate_id(TRUE);
        }
        $model = new Model();
        $user = $model->getUser($username);
        $_SESSION["username"] = $username; // start session, save username.
        $_SESSION['userid'] = $user->id; // start session,save id of user
        redirect("index.php");
    } else {
            redirect("loginpage.php","Dati sbagliati , perfavore reinserisci username o password");
    }
}
?>
