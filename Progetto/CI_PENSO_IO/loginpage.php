<?php
    session_start();
    require_once 'MVC/MODEL/session/session_function.php';
    check_to_index();
    require_once 'MVC/CONTROLLER/loginpagecontroller.php';
    ?>
<!DOCTYPE html>
    <html lang="it">
    <head>
        <title>CPI</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">
        <link href="MVC/VIEW/login/loginstyle.css" type="text/css" rel="stylesheet">
    </head>
    
    <body>
        
        <div id="head">Ci penso io</div>
   <?php
    $controller = new Controller();
    $controller->invoke();
    
    if(isset($_SESSION['flash'])){
               
    ?>
        <h2 id="baddata"><?=$_SESSION['flash']?></h2>
        
<?php
    unset($_SESSION['flash']);
    }
    ?>
    </body>
</html>