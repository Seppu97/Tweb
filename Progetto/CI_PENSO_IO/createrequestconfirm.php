<?php
/* this page return : FALSE = invalid data or Internal Error, 1 = creataion done correctly , -1 = too few cpi to request it.*/
session_start();
require_once 'MVC/MODEL/model.php';

$model = new Model();
$response = FALSE; 

//filter_input return TRUE if string is correct , NULL if variabile is not set , FALSE if filter fails.
$title = filter_input(INPUT_POST,'title',FILTER_SANITIZE_STRING);
$categoryid = filter_input(INPUT_POST,'categoryid',FILTER_VALIDATE_INT);
$expiry_post = filter_input(INPUT_POST,'expiry',FILTER_SANITIZE_STRING);
$expiry =  DateTime::createFromFormat('Y-m-d',$expiry_post  );
if( DateTime::getLastErrors()['warning_count'] > 0 ){
    $expiry = FALSE;
}
$description = filter_input(INPUT_POST,'description',FILTER_SANITIZE_STRING);

//data of user and service required ( cpi of user and cpi required from service);
$services = $model->getAllServices();


if($title && $categoryid && $expiry && $description &&  strlen($title) <= 58 && strlen($description) <= 400 
        && $expiry->format("Y-m-d") >= date("Y-m-d") && checkCategoryRange($services, $categoryid) ){
    
    $user = $model->getUser($_SESSION['username']);
    $personalcard = $model->getPersonalcard($user->personalcard);
    $service = $model->getService($categoryid);
    
    if($service->cpi <= $personalcard->cpi){ // user has the necessary cpi
        
       $insert = $model->insertRequest( new Request(-1,$title,$description,date("Y-m-d"),$expiry->format("Y-m-d"),-1,-1,$user->id,$service->id) ); // add new Request
       if($insert){ // insert done correctly
            $personalcard->cpi -= $service->cpi; // subtract cpi to user.
            if($model->updatePersonalcard($personalcard)){// update the new personalcard
                $response = 1;
            }else{
                $response = FALSE;
            }
       }
    }else{
        //user NOT has necessary cpi
        $response = -1;
    }
}

    print json_encode($response);

//END FILE-----------------------------------------------------

//check if the id of category passed with POST method is valid!
function checkCategoryRange($services,$categoryid){
    foreach ($services as $service){
        if($service->id == $categoryid){
            return TRUE;
        }
    }
    return FALSE;
}

?>