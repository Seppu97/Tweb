<?php
    session_start();
    require_once 'MVC/MODEL/session/session_function.php';
    check_logged_in();
?>
<html lang="it">
    <head>
        <title>CPI</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">
        <link href="MVC/VIEW/profile/menu.css" type="text/css" rel ="stylesheet">
        <link href="MVC/VIEW/profile/personalcard.css" type="text/css" rel="stylesheet">
        <script src=" https://ajax.googleapis.com/ajax/libs/prototype/1.7.0.0/prototype.js " type="text/javascript"></script>
    </head>
    <body>
        <?php
        require_once 'MVC/CONTROLLER/profilecontroller.php';
        $controller = new Controller();
        $controller->invoke();
        ?>
    </body>
</html>
 

