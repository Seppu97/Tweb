//this is the js file for accepting an offer for use'rs requests
var user;
var personalcard;
var opened = false;
var userid;
var vote = null;
//_____________________________
var accepted = false;
var input; //used to get the reference to input box cliccked ( help me to know right offerid value)

window.onload=function(){
    var buttons = $$(".confirmbutton");
    for(var i = 0 ; i < buttons.length; i++){
        buttons[i].observe("click",showmodal);
}
   //___________________________________
    var titles = $$(".username");    
    for(i = 0 ; i < titles.length; i++){
        titles[i].observe("click",getUser);  //personalcard 
    }
   //_______________________________
};

function showmodal(event){
    input = this;
    $("modal").addClassName("modal-visible");
    $$(".close")[0].observe("click",function(){
                                      closeModal();
                                    });
                                    
    window.onclick = function(event) {
        if (event.target === $("modal")) {
          closeModal();
        }
    };
    
    $$(".button")[0].observe("click",accept);
    $$(".button")[1].observe("click",closeModal);
}

function accept(){
  var offerid = input.parentElement.readAttribute("data-value");
    new Ajax.Request("acceptoffer.php",
            {
            method:"POST",
            parameters: {offerid:offerid},
            onSuccess: acceptedOffer,
            onFailure: insertFailed,
            onException: insertFailed
            }
        );
}

function acceptedOffer(ajax){
    var response = JSON.parse(ajax.responseText);
    if(response){
        $("paragraph").addClassName("invisible");
        $$(".close")[0].insert({after: "<p id='acceptparagraph'>Proposta accettata correttamente</p>"});
        accepted = true;
    }else{
        $("paragraph").addClassName("invisible");
        $$(".close")[0].insert({after: "<p id='acceptparagraph'>Problema interno,si prega di avere pazienza </p>"});
    }
}
function closeModal(){
      $("modal").removeClassName("modal-visible");
      $("paragraph").removeClassName("invisible");
      var p = $("acceptparagraph");
      if(p){
        $("acceptparagraph").remove();
      }
      if(accepted){
              location.href="personalrequestpage?mode=1";
          }
}
function insertFailed(ajax){
    $("paragraph").addClassName("invisible");
    $$(".close")[0].insert({after: "<p id='acceptparagraph'>Problema interno,si prega di avere pazienza </p>"});
}

//personal card ________________________________________________________

function showPersonalCard(event){
    $("wrapper").removeClassName("invisible");
    $("wrapper").addClassName("visible");
}

function hidePersonalCard(){
    event.preventDefault();
    $("wrapper").addClassName("invisible");
    $("wrapper").removeClassName("visible");
    var rate = $("rate");
    if(rate){
        $("rate").removeClassName("visible");
        $("rate").addClassName("invisible");
    }
    
    var voted =$("voted");
    if(voted){
        voted.remove();
    }
}

function getUser(event){
    event.preventDefault();
    userid = this.getAttribute("data-value");
    $("close").observe("click",hidePersonalCard);
      new Ajax.Request("userjson.php",
            {
            method:"POST",
            parameters: {userid:userid},
            onSuccess: getPersonalCard,
            onFailure: ajaxFailed,
            onException: ajaxFailed
            }
        );
}

function getPersonalCard(ajax){
    user = JSON.parse(ajax.responseText);
    if(user !== null && user !== false){
        new Ajax.Request("personalcardjson.php",
            {
            method:"POST",
            parameters: {personalcard_id:user.personalcard},
            onSuccess: fillFields,
            onFailure: ajaxFailed,
            onException: ajaxFailed
            }
        );
    }else{
        window.location="errorpage.php?error=2";
    }
}

function fillFields(ajax){
    personalcard = JSON.parse(ajax.responseText);
  
    if(user !== null && user !== false && personalcard!== false && personalcard!== null){
        
        $("username").innerHTML = user.username;
        $("cpi").innerHTML = personalcard.cpi;
        $("n_contributions").innerHTML = personalcard.n_contributions;
        if(personalcard.rating > 0){
            var rate = personalcard.rating/personalcard.n_rate;
            if (rate % 1 === 0){
                $("rating").innerHTML = rate + "/5";
            }else{
                $("rating").innerHTML = rate.toFixed(1)+"/5";
            }
        }else{
            $("rating").innerHTML = 0;
        }
        $("firstname").innerHTML = user.firstname + user.lastname;
        $("email").innerHTML = user.email;
        $("phone").innerHTML = user.phone;
        $("study").innerHTML = personalcard.study;
        if(opened === true){
            hidePersonalCard();
        }
        
        checkVote();
        window.setTimeout(function(){showPersonalCard();opened = true;},100);
        
        
    }else{
        window.location="errorpage.php?error=2";
    }
   
}

function checkVote(){
     new Ajax.Request("checkvote.php",
            {
            method:"POST",
            parameters: {userid:userid},
            onSuccess: showRate,
            onFailure: ajaxFailed,
            onException: ajaxFailed
            }
        );
}

function showRate(ajax){
    var response = JSON.parse(ajax.responseText);
    if(response === true){ // you can vote
        $("rate").removeClassName("invisible");
        $("rate").addClassName("visible");
        
        $("vote").observe("click",insertVote);
        var star_clicked = $$(".star_clicked");
        for(var i = 0 ; i < star_clicked.length; i++){
            star_clicked[i].observe("click",setVote);
        }
    }else if(response === false){
        window.location="errorpage.php?error=2";
    }else if(response === -1){
        $("study").insert({after:"<p id='voted'>Già Votato</p>"});
    }
   
  
}

function insertVote(){
    if(vote && vote <= 5 && vote >=1){
        new Ajax.Request("insertvotejson.php",
            {
                method:"POST",
                parameters: {voted:userid,rate:vote},
                onSuccess: doneVote,
                onFailure: ajaxFailed,
                onException: ajaxFailed
            }
        );
    }else{
        $("modal").addClassName("modal-visible");
        $("paragraph").addClassName("invisible");
        $$(".close")[0].insert({after: "<p id='acceptparagraph'>Specificare un voto </p>"});
        $$(".close")[0].observe("click",hideModal);
    }
}

function setVote(event){
    vote = this.getValue();
}

function hideModal(event){
     $("modal").removeClassName("modal-visible");
      $("paragraph").removeClassName("invisible");
      var p = $("acceptparagraph");
      if(p){
        $("acceptparagraph").remove();
      }
     
}

function doneVote(ajax){
    var response = JSON.parse(ajax.responseText);
    
    $("modal").addClassName("modal-visible");
    $$(".close")[0].observe("click",hideModal);
    
    if(response === false){
         $("paragraph").addClassName("invisible");
        $$(".close")[0].insert({after: "<p id='acceptparagraph'>impossibile votare</p>"});
    }else if (response === -1){
         $("paragraph").addClassName("invisible");
        $$(".close")[0].insert({after: "<p id='acceptparagraph'>Hai già votato</p>"});
    }else if (response){
         $("paragraph").addClassName("invisible");
        $$(".close")[0].insert({after: "<p id='acceptparagraph'>Voto effettuato correttamente </p>"});
         
            var rate = response.rating/response.n_rate;
            if (rate % 1 === 0){
                $("rating").innerHTML = rate + "/5";
            }else{
                $("rating").innerHTML = rate.toFixed(1)+"/5";
            }
            
            $("rate").removeClassName("visible");
            $("rate").addClassName("invisible");
        }
    
}
function ajaxFailed(ajax){
    window.location="errorpage.php?error=2";
}