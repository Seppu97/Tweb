var existusername = false;
var existemail = false;
var existidentity = false;

window.onload=function(){
    $("registrati").observe("click",checkForm);
    $("user").observe("click",focusUser);
    $("email").observe("click",focusEmail);
    $("identitycard").observe("click",focusIdentity);
    
};



function checkForm(){
     if(checkFilled()){
        new Ajax.Request("userjson.php",
            {
            method:"POST",
            parameters: {username:$("user").getValue(),email:$("email").getValue(),identitycard:$("identitycard").getValue()},
            onSuccess: checkUser,
            onFailure: ajaxFailed,
            onException: ajaxFailed
            }
        );
    }
}

function checkUser(ajax){  // this function is called when the specific user is returned from db
    var user = JSON.parse(ajax.responseText);
    
     if( user === null && user !== false){// not already exist this user and request done correctly(i can register him)
           var data =  getData();
           
             new Ajax.Request("registerconfirm.php",
                {
                method:"POST",
                parameters: {identitycard:data[7],username:data[0],password:data[1],firstname:data[2],
                    lastname:data[3],birthday:data[5],email:data[4],phone:data[6]},
                onSuccess: checkRegister,
                onFailure: ajaxFailed,
                onException: ajaxFailed
                }
            );
            
    }else if(user === false){
        location.href="errorpage.php?error=2";
    }else{
            if(checkExistUsername(user) && !existusername){
                existusername = true;
                $("user").addClassName("login-input-bad");
                $("user").insert({before: "<span id='badusername'>username già esistente!</span>"});
                scroll(0,0);
            }
            
            if(checkExistEmail(user) && !existemail){
                existemail = true;
                $("email").addClassName("login-input-bad");
                $("email").insert({before: "<span id='bademail'>email già esistente!</span>"});
            }
            
            if(checkExistIdentity(user) && !existidentity){
                existidentity = true;
                $("identitycard").addClassName("login-input-bad");
                $("identitycard").insert({before: "<span id='badidentity'>carta d'identità già esistente!</span>"});
            }
         }
         
    
}

function ajaxFailed(ajax){
    alert("errore interno");
}
//this function check if all input text are filled 
function checkFilled(){
    var inputs = $$(".login-input"); // take all elements with class = login-input
    for(var i = 0 ; i < inputs.length; i++){
        if(inputs[i].reportValidity() === false || inputs[i].getValue() === ""){
            return false;
        }
    }
    return true;
}

function checkRegister(ajax){
    var response = JSON.parse(ajax.responseText);
    if(response === true){
        $("head").insert({after:"<h1 id='notice'>Registrazione avvenuta con successo, verrai mandato immediatamente alla pagina di login</h1>"});
        //$("notice").innerHTML="Registrazione avvenuta con successo, verrai mandato immediatamente alla pagina di login";
        scroll(0,0);
        window.setTimeout("redirect()", 2200);
    }else if(response === null){
        $("head").insert({after:"<h1 id='notice'>Assicurarsi di aver inserito i dati corretti</h1>"});
        //$("notice").innerHTML="Assicurarsi di aver inserito i dati corretti";
        scroll(0,0);
    }else{
        location.href="errorpage.php?error=2";
    }
 }

function getData(){
    var data = new Array();
    var inputs = $$(".login-input"); // take all elements with class = login-input
    for(var i = 0 ; i < inputs.length; i++){
      data[i] = inputs[i].getValue();
     
    }
    
    return data;
}

function focusUser(){
    if(existusername){
        existusername = false;
        $("badusername").remove();
        $("user").removeClassName("login-input-bad");
    }
}

function focusEmail(){
    if(existemail){
        existemail = false;
        $("bademail").remove();
        $("email").removeClassName("login-input-bad");
    }
}

function focusIdentity(){
    if(existidentity){
        existidentity = false;
        $("badidentity").remove();
        $("identitycard").removeClassName("login-input-bad");
    }
}

function checkExistUsername(user){
    var username = $("user").getValue();
    for (var i = 0 ; i < user.length; i++){
        if(user[i].username === username){
           return true;
        }
    }
    return false;
}

function checkExistEmail(user){
    var email = $("email").getValue();
    for (var i = 0 ; i < user.length; i++){
        if(user[i].email === email){
           return true;
        }
    }
    return false;
}

function checkExistIdentity(user){
    var identitycard = $("identitycard").getValue();
    for (var i = 0 ; i < user.length; i++){
        if(user[i].identitycard === identitycard){
           return true;
        }
    }
    return false;
}

function redirect(){
    location.href = "loginpage.php";
}

//used during development fase for save myself
function autofill(){
    var fields = $$(".login-input");
    fields[0].value="mattew";
    fields[1].value="Cipolla-2";
    fields[2].value="matteo";
    fields[3].value="cipolla";
    fields[4].value="matteo@gmail.com";
    fields[6].value="3314612423";
    fields[7].value="QWERG";
}