window.onload= function(){
    $("conferma").observe("click",submit);
    $("title").observe("click",writing);
    $("description").observe("click",writing);
};

function submit(){
    var fill= checkFilled();
    var length = checkLength();
    var categories = checkCategories();
    if(fill && length && categories){
        
        new Ajax.Request("createrequestconfirm.php",
            {
            method:"POST",
            parameters: {title:$("title").getValue(),categoryid:$("category").getValue(),
                description:$("description").getValue(),expiry:$("expiry").getValue()},
            onSuccess: checkResponse,
            onFailure: ajaxFailed,
            onException: ajaxFailed
            }
        );
    
    }else if (!length || !categories){
        $("head").innerHTML = "I dati non rispettano il formato richiesto!";
        $("head").addClassName("baddata");
    }
        
}

function checkResponse(ajax){
    var response = JSON.parse(ajax.responseText);
    
    if(response === false){
        $("head").innerHTML = "I dati non rispettano il formato richiesto!";
        $("head").addClassName("baddata");
    }else if (response === -1){
        $("head").innerHTML = "Non possiedi i cpi necessari per pubblicare l'annuncio , contribuisci a ci penso io ";
        $("head").addClassName("baddata");
    }else if (response === 1){
        $("head").innerHTML = "Richiesta pubblicata correttamente, grazie di far parte della community.";
          window.setTimeout(function () {
        location.href = "index.php";
    }, 2200);
    }
}

function checkFilled(){
   return( $("title").reportValidity() && $("category").reportValidity() && 
           $("description").reportValidity() && $("expiry").reportValidity());
      
   }
   
function checkLength(){
    var title = $("title").getValue();
    var description = $("description").getValue();
    
    return (title.length <= 58 && description.length <= 400 );
}

function checkCategories(){
    var categories = $$(".category");
    for(var i = 0; i < categories.length - 1 ; i++){
        if(parseInt(categories[i].value) >= parseInt(categories[i+1].value))
            return false;
    }
    
    return true;
}
function ajaxFailed(ajax){
    alert("problema interno");
    //location.href="errorpage.php?error=2";
}

function writing(){
    $("head").innerHTML = "Crea la tua richiesta , al resto ci penso io";
    $("head").removeClassName("baddata");
}