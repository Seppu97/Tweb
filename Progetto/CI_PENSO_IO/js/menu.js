/*
var user;
var personalcard;
var opened = false;

window.onload = function(){
    $("dropdownprofile").observe("mouseover",changeIcon);
    $("dropdownprofile").observe("mouseout",resetIcon);
    
    
    var titles = $$(".username");    
    for(i = 0 ; i < titles.length; i++){
        titles[i].observe("click",getUser);
    }
    
};
function changeIcon(event){
    $("profileimage").src="img/profile2.png";
}

function resetIcon(event){
    $("profileimage").src="img/profile.png";
}

//____________________________________________
//personal card function
function showPersonalCard(event){
    $("wrapper").removeClassName("invisible");
    $("wrapper").addClassName("visible");
}

function hidePersonalCard(){
    event.preventDefault();
    $("wrapper").addClassName("invisible");
    $("wrapper").removeClassName("visible");
}

function getUser(event){
    event.preventDefault();
    var userid = this.getAttribute("data-value");
    $("close").observe("click",hidePersonalCard);
      new Ajax.Request("userjson.php",
            {
            method:"POST",
            parameters: {userid:userid},
            onSuccess: getPersonalCard,
            onFailure: ajaxFailed,
            onException: ajaxFailed
            }
        );
}

function getPersonalCard(ajax){
    user = JSON.parse(ajax.responseText);
    if(user !== null && user !== false){
        new Ajax.Request("personalcardjson.php",
            {
            method:"POST",
            parameters: {personalcard_id:user.personalcard},
            onSuccess: fillFields,
            onFailure: ajaxFailed,
            onException: ajaxFailed
            }
        );
    }else{
        window.location="errorpage.php?error=2";
    }
}

function fillFields(ajax){
    personalcard = JSON.parse(ajax.responseText);
  
    if(user !== null && user !== false && personalcard!== false && personalcard!== null){
        
        $("username").innerHTML = user.username;
        $("cpi").innerHTML = personalcard.cpi;
        $("n_contributions").innerHTML = personalcard.n_contributions;
        if(personalcard.rating > 0){
            var rate = personalcard.rating/personalcard.n_rate;
            if (rate % 1 === 0){
                $("rating").innerHTML = rate + "/5";
            }else{
                $("rating").innerHTML = rate.toFixed(1)+"/5";
            }
        }else{
            $("rating").innerHTML = 0;
        }
        $("firstname").innerHTML = user.firstname + user.lastname;
        $("email").innerHTML = user.email;
        $("phone").innerHTML = user.phone;
        $("study").innerHTML = personalcard.study;
        if(opened === true){
            hidePersonalCard();
        }
        window.setTimeout(function(){showPersonalCard();opened = true;},100);
        
        
    }else{
        window.location="errorpage.php?error=2";
    }
   
}

function ajaxFailed(ajax){
    window.location="errorpage.php?error=1";
}*/