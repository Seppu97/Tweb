//this file takes care of inserting a new user's offer in the database or to modify its current availability 
firstaxcess = true;
window.onload=function(){
    var cipensoio = $("submit");
    if(cipensoio){
        $("submit").observe("click",cipensoclick);
    }
    
    var editButton = $$(".editbutton")[0];
    
    if(editButton){
        editButton.observe("click",showEditDate);
    }
    
    var confirmModify = $("confirm_modify");
    if(confirmModify){
        confirmModify.observe("click",updateDate);
    }
};

function cipensoclick(){ //manage the click of cipensoio button.
    if(firstaxcess){
      $("invisible").removeClassName("invisible");
      $("invisible").addClassName("visible");
      firstaxcess = false;
     }else{
         if($("availability").getValue() !== ""){
            new Ajax.Request("offerconfirm.php",
              {
              method:"POST",
              parameters: {availability:$("availability").getValue()},
              onSuccess: checkDate,
              onFailure: ajaxFailed,
              onException: ajaxFailed
              }
           );
         }else{
             $("availability").addClassName("baddate"); //red board
             $("availability").observe("click",removeClass);
         }
     }
        
}

function removeClass(){
    this.removeClassName("baddate");
}
function checkDate(ajax){
    var offer = JSON.parse(ajax.responseText);
  
    if(offer){
        location.reload();
    }else if(offer === null){
        $("availability").addClassName("baddate");
    }else{
        location.href="errorpage.php?error=2";
    }
    
}

function updateDate(event){
   new Ajax.Request("updateDateOffer.php",
              {
              method:"POST",
              parameters: {availability:$("availability_input").getValue()},
              onSuccess: checkUpdateDate,
              onFailure: ajaxFailed,
              onException: ajaxFailed
              }
           );
}

function checkUpdateDate(ajax){
    var response = JSON.parse(ajax.responseText);
    //response is the new date of offer
    if(response){
        hideEditDate(response);
    }else if(response === null){
        $("availability_input").addClassName("baddate");
        $("availability_input").observe("click",removeClass);
    }else if(response === false){
        location.href="errorpage.php?error=2";
    }
}

function showEditDate(event){
    $("availability_text").removeClassName("visible_inline");
    $("availability_text").addClassName("invisible_inline");
    var input_date = $$(".invisible_inline")[1];
    input_date.removeClassName("invisible_inline");
    input_date.addClassName("visible_inline");
}

function hideEditDate(newDate){
    $("availability_text").removeClassName("invisible_inline");
    $("availability_text").addClassName("visible_inline");
    $("date_offer").innerHTML="Data: "+ String(newDate);
    var input_date = $$(".visible_inline")[1];
    input_date.removeClassName("visible_inline");
    input_date.addClassName("invisible_inline");
}

function ajaxFailed(ajax){
    alert("chiamata fallita");
}
