<?php
// return FALSE = validation error , NULL = card not found or $personalcard_id incorrect

require_once 'MVC/MODEL/function_db/query.php';
require_once 'MVC/MODEL/classes/Personalcard.php';

$personalcard_id = filter_input(INPUT_POST, 'personalcard_id',FILTER_VALIDATE_INT);
$personalcard = NULL;

if($personalcard_id){
    $rows = personalCardQuery($personalcard_id);
    foreach($rows as $row){
        $personalcard = new Personalcard(
                                $row['id'], 
                                $row['study'], 
                                $row['cpi'], 
                                $row['rating'], 
                                $row['n_rate'], 
                                $row['n_contributions']
                            );
    }
}elseif($personalcard_id === FALSE){
    $personalcard = FALSE;
}

print json_encode($personalcard); 

?>
