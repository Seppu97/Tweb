<?php
//this page return : TRUE = user can vote, FALSE = internal error, NULL = user not done any offers, -1 user just voted .
session_start();
require_once 'MVC/MODEL/model.php';
$model = new model();
$voted = filter_input(INPUT_POST,'userid',FILTER_VALIDATE_INT);
$voter = $_SESSION['userid'];

if($voted && ($voter !== $voted)){
    $response = $model->canVote($voter,$voted);
}else{
    $response = FALSE;
}

print json_encode($response);

?>

