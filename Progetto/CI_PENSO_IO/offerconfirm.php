<?php
/*confirm the offer made by user for the specific request
this page return  FALSE for invalid formate date or other error, NULL if the date is not in range,
Offer object is returned if has been inserted correctly*/
session_start();
require_once 'MVC/MODEL/model.php';

$model = new Model();
$response = NULL;

$availability_post = filter_input(INPUT_POST,'availability',FILTER_SANITIZE_STRING);
//validation of date using default php Class ( DateTime ).
$availability =  DateTime::createFromFormat('Y-m-d',$availability_post);
if( DateTime::getLastErrors()['warning_count'] > 0 ){
     $response = FALSE;
     
 }

 // every time user acces to details of a requests , such data are stored into session to allow access 
                                                                  // from offerconfirm.php (this page)
$expiry = $_SESSION['expiry'];
$request = $_SESSION['request'];
$userid = $_SESSION['userid'];

$date = date("Y-m-d");
if($response !== FALSE){
    $offer = new Offer(-1,$availability_post,$date,0,0,$userid,$request);
    
    if( ($availability_post >= $date) && ($availability_post <= $expiry) ){
        
       $insert = $model->insertOffer($offer);
       if($insert){
           $response = $offer;
       }else{
           $response = FALSE;
       }
    }else{
       $response = NULL;
    }
}

print json_encode($response);
 
 

?>
