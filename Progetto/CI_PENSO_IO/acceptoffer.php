<?php
// this page return TRUE is offer is correctly accepted , FALSE otherwise
session_start();
require_once 'MVC/MODEL/model.php';
$offerid = filter_input(INPUT_POST,'offerid',FILTER_VALIDATE_INT);

$model = new Model();

if($offerid){
    $response = $model->acceptOffer($offerid);
}else{
    $response = FALSE;
}

print json_encode($response);
?>