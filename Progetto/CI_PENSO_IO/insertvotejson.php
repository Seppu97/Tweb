<?php
//this page return new Personalcard if voted user if vote is correctly inserted,-1 if you already voted user , FALSE for internal error.
session_start();
require_once 'MVC/MODEL/model.php';

$model = new model();
$voter = $_SESSION['userid'];
$voted = filter_input(INPUT_POST,'voted',FILTER_VALIDATE_INT);
$rate = filter_input(INPUT_POST,'rate',FILTER_VALIDATE_INT);

if($voter && $voted ){
    $response = $model->canVote($voter, $voted);
}else{
    $response = FALSE;
    
}


if($voter && $voted  && $response && $response !== -1){
    if($rate && $rate >= 1 && $rate <= 5){
        $response = $model->insertVote($voter, $voted, $rate);
    }else{
        $response = NULL;
        
    }
}elseif($response === -1){
    $response = -1;
}else{
    $response = FALSE;
}

if($response === TRUE){
    $user = $model->getUserById($voted);
    $response = $model->getPersonalcard($user->personalcard); //get the new personalcard of voted user.
}
print json_encode($response);
?>
