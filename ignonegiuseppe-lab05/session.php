<?php
/*this file contain all function for the managing of session*/

function is_password_correct($username,$password) {
    $db = new PDO('mysql:dbname=imdb_small;host=localhost','root','');
    $username = $db->quote($username);
    $rows = $db->query("SELECT password FROM user WHERE username like $username");
    if ($rows) {
		foreach ($rows as $row) {
			$correct_password = $row["password"];
			return $password === $correct_password;
			
		}
    } else {
		return FALSE; //user note found
    }
}

function check_logged_in() {
 if (!isset($_SESSION["username"])) {
    redirect("user.php");
 }
}

function redirect($url, $flash_message = NULL,$refreshtime = 0) {
    if ($flash_message) {
        $_SESSION["flash"] = $flash_message;
    }
    if($refreshtime == 0){
        header("Location: $url");
    }else{
        header("Refresh:$refreshtime;url='$url'");
    }
    die;
}

?>
