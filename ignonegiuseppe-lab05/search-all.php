<?php
//this file is for searching all film of a single actor
include("top.php");
include("common.php");

$name=$_GET['firstname'];
$surname=$_GET['lastname'];

try{
    $db = new PDO("mysql:dbname=imdb_small;host=localhost","root","");
    $rows = moviesOfActor($name, $surname,$db);
    
}catch(PDOException $ex){
    print"Sorry, a database error occurred. Please try again later.";
    print"Error details: $ex->getMessage()";
}

if($rows->rowCount() > 0){
    $i =1;
?>
<h1>Result for <?=$name." ".$surname?></h1>

<table>
    <caption>
	<p>Only film of <?=$name." ".$surname?></p>
    </caption>
        <tr><th id="firstcol">#</th><th>Title</th><th id="lastcol">Year</th></tr>
        <?php
            foreach($rows as $row){
        ?>
        <tr><td><?=$i?></td><td><?=$row['name']?></td><td><?= $row['year']?></td></tr>

<?php
    $i++;
    }
 ?>
</table>
<?php
}else{
?>
<h2>Actor <?=$name." ".$surname?> Not Found</h2>
<?php
}
?>

<?php

include("bottom.html");
?>

