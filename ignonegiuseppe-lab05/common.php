<?php
/* NOTE: my function return a subquery string ( not an integer id). i build the final query bu putting the subquery togheter,
         and in the end i query the database with it.
 */
function minID($name,$surname,$db){ /* select the min id of the actors with max number of film_count that have the specific name and surname */
    $qname=$db->quote($name." (%)");
    $qsurname=$db->quote($surname);
    $name=$db->quote($name);
    $query="select min(distinct(actors.id)) 
                        from actors join roles on actors.id = roles.actor_id 
                        where (actors.first_name like $qname or actors.first_name like $name ) and actors.last_name like $qsurname and actors.film_count = (select max(actors.film_count) 
                                                   from actors join roles on actors.id = roles.actor_id 
                                                   where (actors.first_name like $qname or actors.first_name like $name ) and actors.last_name like $qsurname )";
    return $query;
}


function moviesOfActor($name,$surname,$db){ /* return the final table of specific actor's movies */
    $actorID = minID($name,$surname,$db); // return the min id of actors.
   
    $finalquery="select movies.name,movies.year
                  from movies join roles on movies.id = roles.movie_id 
                  where roles.actor_id = ($actorID)
                  order by movies.year desc,movies.name asc"; // return a movies table ordered by year
        
    return $db->query($finalquery);
}


function moviesOfActorKevin($name,$surname,$db){  /* return the final tavle of movies done by specific actor and Kevin Bacon togheter */
    
    $actorID= minID($name, $surname,$db);
    $kevinID= minID("Kevin","Bacon",$db);
    
    $finalquery="select movies.name,movies.year
            from roles r1 join roles r2 on r1.actor_id <> r2.actor_id and r1.movie_id = r2.movie_id
 			  join movies on movies.id = r1.movie_id
            where r1.actor_id =($actorID) and r2.actor_id=($kevinID)
            order by movies.year desc,movies.name asc";
    
    return $db->query($finalquery);
}


?>