<?php
//this file is for searching all film of a single actor
include("top.php");
include("common.php");

 $name = $_GET['firstname'];
 $surname = $_GET['lastname'];
 
 try{
    $db = new PDO("mysql:dbname=imdb_small;host=localhost","root","");
    $rows = moviesOfActorKevin($name,$surname,$db);
    
}catch(PDOException $ex){
    print"Sorry, a database error occurred. Please try again later.";
    print"Error details: $ex->getMessage()";
}
?>

<?php
if($rows->rowCount() > 0){
    $i =1;
?>
<h1>Result for Kevin Bacon</h1>

<table>
    <caption>
	<p>Film of <?=$name." ".$surname?> with Kevin Bacon</p>
    </caption>
        <tr><th id="firstcol">#</th><th>Title</th><th id="lastcol">Year</th></tr>
        <?php
            foreach($rows as $row){
        ?>
        <tr><td><?=$i?></td><td><?=$row['name']?></td><td><?= $row['year']?></td></tr>
</table>
<?php
    $i++;
    }
}else{
?>
<h2> <?=$name." ".$surname?> wasn't in any film with Kevin Bacon</h2>
<?php
}
?>

<?php

include("bottom.html");
?>