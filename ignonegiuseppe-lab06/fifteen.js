//Global variables :
var freeBlock; // used to store the position of white block
var oldfreeBlock;// used to store the old prosition of withe block (before to move it)
var win = false; // used to check if user has win or not ( if not i don't do algorithm for checking victory etc...)
var startedgame = true; // used to check if game is started (thi default is true but when i start the SHUFFLE i disable temporarily the game)

window.onload=function(){
    setupBlocks();
    setBlocksEvent();
    $("shufflebutton").observe("click",shuffle);
    createWinnerBanner();  
};
 

function setupBlocks(){
    setBlocksId();
    setBlocksBackground();
    oldfreeBlock = freeBlock = 44;
}

function setBlocksId(){
    var area=$$("#puzzlearea div");
    var row = 1;
    for(var i = 0 ; i < area.length ; i++){
        var col = (i%4) + 1;
        area[i].setAttribute("id",("block_" + row + "_" + col));
        area[i].addClassName("cannotmove");
        
        if(col === 4){
            row++;
        }
    }
}

function setBlocksBackground(){
    var area=$$("#puzzlearea div");
    var top = 0;
     for(var i = 0 ; i < area.length ; i++){ 
        var right = (i % 4) * 100;  //offset = Math.sqrt((widthimage*heightimage)/numberofBlocks);
        area[i].style.backgroundPosition="-"+right+"px"+" "+top+"px"; 
        if(right === 300){
            top = top - 100;
        }
    }
}

function setBlocksEvent(){
    var area = $$("#puzzlearea div");
    for(var i = 0 ; i < area.length; i++){
        area[i].observe("click",moveBlock);
        area[i].observe("mouseover",mouseOverBlock);
        area[i].observe("mouseout",mouseOutBlock);
    }
    
}

function canMove(element){
    var currentElem = element.getAttribute("id");
    var currentPos= parseInt(currentElem.charAt(6) + currentElem.charAt(8));//number of block identifier
    if((currentPos === freeBlock - 10) || (currentPos === freeBlock + 10) //if is an adjacent of white "block"
        || (currentPos === freeBlock - 1) || (currentPos === freeBlock + 1)){
        
        return currentPos;
    }
    return 0; //if isn't and adjacent return 0
}

function mouseOverBlock(event){
    if(canMove(this)){
        this.removeClassName("cannotmove");
        this.addClassName("canmove");
    }
}

function mouseOutBlock(event){
    if(canMove(this)){
        this.removeClassName("canmove");
        this.addClassName("cannotmove");
    }
}

function moveBlock(){
    if(!win){
        var currentPos = canMove(this);
        //freeblock/10 is the first number, freeblock % 10 is the second number. ex : 44 -> 4 4
        var freeBlockString = "block_" + parseInt(freeBlock/10) + "_" + freeBlock%10;

        if(currentPos){ //currentPos is the id that you can move
            this.writeAttribute("id",freeBlockString);
            oldfreeBlock=freeBlock;
            freeBlock = currentPos; // update new free position
            if(startedgame){
                showWin();  // check victory only if game is started
            }
        }
    }
  
}

function shuffle(){
    startedgame = false;
    var randoms = new Array(-1,1,-10,10); //array of 4 possibile adjacent block (that you can move )
    var n = Math.random()* 26 + 8;  //number of automatic moves (algorithm mix the  blocks)
    while(n > 0){
        var randoms = shuffleArray(randoms); // i still add randomness
        var i = 0;
        // if the random block chosen is not in the range of matrix index try with next random block.
        //  with the secondo control of expression, shuffle algorithm CAN NOT make two consecutive equal moves
        // example : can't do block_4_3 - > block_4_4 and then block_4_4 -> block_4_3 (it's a WASTED MOVE)
        while(!isInRange(freeBlock + randoms[i]) || (freeBlock + randoms[i]) === oldfreeBlock){ 
            i++;
        }
        var newBlockNumber=parseInt(freeBlock+randoms[i]);
        var newBlock = "block_"+parseInt(newBlockNumber/10)+"_" + parseInt(newBlockNumber%10);
        $(newBlock).click(); // simulating click of the block
        n--;
    }
    
    startedgame = true;
}

function isInRange(number){
    //i control the limits of virtual matrix
    if(number > 5 && number < 50 && number%10 != 0 && number%5 != 0){
        return true;
    }
    return false;
}

function shuffleArray(array) {
    /* this function shuffle the array of possibile adjacent blocks. 
        this allow to choice everytime a random adjacent block and to move it.*/
    var counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // take random index in [0,counter-1];
        var index = Math.floor(Math.random() * counter);
        
        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        var temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

function checkVictory(){
    var area = $$("#puzzlearea div");
    for(var i = 0 ; i < area.length -1 ; i++){
        var id1 = parseInt(area[i].getAttribute("id").charAt(6) + area[i].getAttribute("id").charAt(8));
        var id2 = parseInt(area[i+1].getAttribute("id").charAt(6) + area[i+1].getAttribute("id").charAt(8));
        
        if(id1 > id2 || id2 === 44){ //if id2 is 44,blocks are orderdered BUT it same the white block is in the middle!(not win)
            return false;
        }
    }
    return true;
}

function showWin(){
    if(checkVictory() ){
        win = true;
        Sound.play("win.mp3");
        $("banner").show();
    }
}

function createWinnerBanner(){
    //create the banner of victory and then hide it.
    var button = document.getElementById('shufflebutton');
    var new_node = document.createElement("div");
    new_node.innerHTML = "YOU WIN";
    new_node.setAttribute("id","banner");
    var new_button = document.createElement("button");
    new_button.innerHTML = "New Game";
    new_button.observe("click",newgame);
    new_node.appendChild(new_button);
    button.parentNode.insertBefore(new_node,button.nextSibling);
    new_node.hide();
}

function newgame(){
    window.location.reload();
}